

clear all

%% Import data
X = importdata('zip.train');

Y = X(:, 1);
X = X(:, 2:end);

n_per_digits = 30;
temp_X = [];
temp_Y = [];
for i = 0:9
    X_i = X(Y == i, :);
    kept = min(n_per_digits, size(X_i, 1));
    temp_X = [temp_X; X_i(1:n_per_digits, :)];
end

X = temp_X;
%n = 8;
%X = X(1:n, :);
[n, d] = size(X);

%% Compute Gram Matrix and center it

type1 = 'linear'

sigma2 = 50;
type2 = ['gaussian sigma2=' num2str(sigma2)];

c = 0;
d = 2;
type3 = ['polynomial c=' num2str(c) ', d='  num2str(d) ];

d_log = 6;
type4 = ['log d=' num2str(d_log)];

types = cell(4, 1);
types{1} = type1;
types{2} = type2;
types{3} = type3;
types{4} = type4;

figure;
for typei = 1:length(types)
    type = types{typei}
    name = ['data_' type num2str(n_per_digits)];
    if exist([name '.mat'], 'file')
       load([name '.mat']);
    else
        if (~ isempty(strfind(type, 'gaussian')))
            K = gaussian_kernel(X, sigma2);
        elseif (~ isempty(strfind(type, 'polynomial')))
            K = polynomial_kernel(X, c, d);
        elseif (~ isempty(strfind(type, 'log')))
            K = log_kernel(X, d_log);
        elseif (~ isempty(strfind(type, 'linear')))
            K = linear_kernel(X);
        else
            clear K;
        end
                
        
        H = eye(n) - 1/n.*(ones(n));
        K_tilde = H*K*H;

        %% Get eigen Vectors
        [U, Lambda] = eig(K_tilde);

        %% Reorder the eigen values and corresponding eigen vectors
        [lambda, order] = sort(diag(Lambda), 'descend');
        Lambda = diag(lambda);
        U = U(:, order);

        Alpha = ones(n,n)./repmat(sqrt(lambda'), n, 1) .* U;

        save(name, 'K', 'U', 'Lambda', 'lambda', 'Alpha', 'K_tilde')
    end

    axis1 = Alpha(:, 1)'*K_tilde;
    axis2 = Alpha(:, 2)'*K_tilde;
    
    subplot(2,2, typei)
    colormap jet
    hold on
    markers = ['+','o','*','x','s','d'];
    for digit = 0:9
        digit_range = 1+digit*n_per_digits:(digit+1)*n_per_digits;
        scatter(axis1(digit_range), axis2(digit_range), ...
            'marker', markers(mod(digit,length(markers)) + 1));
    end
    hold off
    title(type, 'FontSize', 16);
    legend('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Location', 'northwest');
end
