function [ beta ] = beta( x, X, K, lambda, U, q, sigma2 )

n = size(X, 1);

k_x = gaussian_kernel_column( x, X, sigma2 );
H = eye(n) - 1/n.*(ones(n));

k_tilde_x = H * ( k_x - 1/n .* K * ones(n,1));

beta = zeros(q, 1);
for i = 1:q    
    beta(i) = 1/sqrt(lambda(i)) * sum(U(:,i) .* k_tilde_x); 
end

end

    