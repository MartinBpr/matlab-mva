function [ z ] = project_digit(x, q, X, K, lambda, U, sigma2, plot)

if nargin < 7
    plot = false;
end

[n, d] = size(X);

betas = beta( x, X, K, lambda, U, q, sigma2 );

gamma = U(:, 1:q) * betas;
gamma_tilde = gamma + 1/n * (1 - sum(gamma));

z = max(min(normrnd(0, 0.5, 1, d), 1), -1);

converged = false;
tol = 1e-10;
max_iter = 1000;
iter = 0;

while(~ converged)
    iter = iter + 1;
    %if iter > 2
    %old_old_z = old_z;
    %end
    old_z = z;
    
    k_z = gaussian_kernel_column( z, X, sigma2 );
    tmp = gamma_tilde .* k_z;

    z = sum(repmat(tmp, 1, d).*X, 1) ./ sum(tmp);
    
    converged = (norm(z - old_z) < tol);
    %if iter > 2
    %   norm(z - old_old_z) 
    %end
    if((~ converged) && (iter > max_iter))
       warning(['over ' num2str(max_iter) 'iterations (' num2str(norm(z - old_z) < tol) ')']) 
       break
    end
end

if plot
    subplot(1,2,1)
    plot_digit(x)

    subplot(1,2,2)
    plot_digit(z)
end

end

