function [ K ] = gaussian_kernel( X, sigma2 )

N = size(X, 1);
K = zeros(N);

for i = 1:N
   for j = 1:N
       K(i,j) = exp( - norm(X(i, :) - X(j, :), 2)^2 / (2 * sigma2) );
   end
   if mod(i, 250) == 0
      i 
   end
end

end

%{
classdef gaussian_kernel<handle
    
    properties
        sigma2
        X
    end
    
    methods
        function self = gaussian_kernel(X, sigma2)
            self.X = X;
            self.sigma2 = sigma2; 
        end
        
        function [k_ij] = individual_kernel(self, x_i, x_j)
            k_ij = exp( - norm(x_i - x_j, 2) / (2 * self.sigma2) );
        end
        
        function [K_matrix] = K(self)
            K_matrix = self.x
        end
                
    end    
end
%}