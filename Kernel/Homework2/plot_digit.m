function plot_digit( x )

x = 2 - x;
x = reshape(x, 16, 16);
x = x';
colormap gray
imagesc(x);

end

