
clear all

%% Import data
X = importdata('zip.train');

Y = X(:, 1);
X = X(:, 2:end);

n_per_digits = 500;
temp_X = [];
for i = 0:9
    X_i = X(Y == i, :);
    kept = min(n_per_digits, size(X_i, 1));
    temp_X = [temp_X; X_i(1:n_per_digits, :)];
end

X = temp_X;
%n = 8;
%X = X(1:n, :);
[n, d] = size(X);

%% Compute Gram Matrix and center it
sigma2 = 50;

name = ['data_' num2str(n_per_digits)];
if exist([name '.mat'], 'file')
   load([name '.mat']);
else
    K = gaussian_kernel(X, sigma2);
    
    H = eye(n) - 1/n.*(ones(n));
    K_tilde = H*K*H;

    %% Get eigen Vectors
    [U, Lambda] = eig(K_tilde);

    %% Reorder the eigen values and corresponding eigen vectors
    [lambda, order] = sort(diag(Lambda), 'descend');
    Lambda = diag(lambda);
    U = U(:, order);
    
    save(name, 'K', 'U', 'Lambda', 'lambda')
end

%% test on data

X_test = importdata('zip.test');
Y_test = X_test(:, 1);
X_test = X_test(:, 2:end);

%% Denoising


figure;
axis off;
for digit = 0:9
    digit
    q = 80;
    
    X_test_digit = X_test((Y_test==digit), :);
    x = X_test_digit(4, :);


    sigma2_noise = 0.5;
    noise = max(min(normrnd(0, sigma2_noise, 1, d), 1), -1);

    x_noise = max(min(x + noise, 1), -1);

    should_plot = false;
    z = project_digit(x_noise, q, X, K, lambda, U, sigma2, should_plot);

    subplot(3, 10, digit + 1)
    plot_digit(x)
    %title('original')
    set(gca, 'xtick', [], 'ytick', [])

    subplot(3, 10,10 + digit + 1)
    plot_digit(x_noise)
    set(gca, 'xtick', [], 'ytick', [])
    %title('noised')

    subplot(3, 10,20 + digit + 1)
    plot_digit(z)
    set(gca, 'xtick', [], 'ytick', [])
    %title('denoised')
end


%% Influence of d

figure;
subplot(1,1,1)
hold on;
for digit=1:5
x = X_test(digit, :);

iterations = [5:9 10:2:18 20:4:26 40:10:240 250:50:500 500:500:4500 4999];
errors = zeros(1,length(iterations));

sigma2_noise = 0.5;
noise = max(min(normrnd(0, sigma2_noise, 1, d), 1), -1);

x_noise = max(min(x + noise, 1), -1);

count = 0;
for q = iterations
    count = count + 1;
    q

    should_plot = false;
    z = project_digit(x_noise, q, X, K, lambda, U, sigma2, should_plot);
    
    errors(count) = norm(x - z);
end


plot(iterations, errors)
end
hold off;
