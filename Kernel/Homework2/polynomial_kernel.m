function [ K ] = polynomial_kernel( X, c, d )
N = size(X, 1);
K = zeros(N);
for i = 1:N
   for j = 1:N
       K(i,j) =  (sum(X(i, :) .* X(j, :)) +c )^d;
   end
end
end