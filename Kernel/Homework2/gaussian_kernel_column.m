function [ k_x ] = gaussian_kernel_column( x, X, sigma2 )

n = size(X, 1);

diff = repmat(x, n, 1) - X;
norm_diff =  sum(diff .* diff, 2);
k_x = exp(- norm_diff / (2 * sigma2) );

end

