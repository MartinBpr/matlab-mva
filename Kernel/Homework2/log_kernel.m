function [ K ] = log_kernel( X, d )
N = size(X, 1);
K = zeros(N);
for i = 1:N
   for j = 1:N
       K(i,j) =  - log(norm(X(i, :)-X(j, :))^d +1);
   end
end
end