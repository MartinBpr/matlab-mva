clear all

type = 'knn';

if strcmp(type, 'knn')
    p_values = [1, 2, 3, 4, 5, 7 , 9, 12, 15, 20, 30];
else
    p_values = [0.0001, 0.0003, 0.001, 0.003, 0.01, 0.03, 0.1];
end

n = size(p_values, 2);
n_launch = 10;

mean_acu = zeros(n_launch, n);
for i = 1:n
    param = p_values(i)
    for k = 1:n_launch
        [accuracy] = offline_face_recognition(type, param);
        mean_acu(k,i) = accuracy;
    end
end

hold on;
plot(p_values, mean_acu, '--')
plot(p_values, mean(mean_acu, 1), 'LineWidth', 3)

if strcmp(type, 'eps')
    set(gca,'XScale','log')
end
hold off;
