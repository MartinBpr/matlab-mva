function [y] = compute_solution(t, centroids, nodes_to_centroids_map, centroids_to_nodes_map, labels)
        gamma_g = 1e-3;
        num_centroids = size(centroids,1);

        %compute the similarity matrix on the centroids

        W = build_faces_graph('knn', 5, centroids, 2);
        
        %compute the normalized laplacian
        

        
        %sqrtD = ;
        %qW = ;
        %qL = ;
        %regL = ;

        %compute the multiplicities
        %V = zeros(length(labels),1);
        V = zeros(length(centroids_to_nodes_map),1);

        for i = 1:length(centroids_to_nodes_map)
            V(i) = sum(nodes_to_centroids_map == centroids_to_nodes_map(i));
        end
        V = diag(V);

        % compute the labels of the nodes that are currently a
        % representative for a centroid
        sublabs = labels(centroids_to_nodes_map);


        Yl = [sublabs(sublabs ~= 0) == -1, sublabs(sublabs ~= 0) == 1];

        % compute the SHFS solution

        
        W2 = V*W*V;
        D = diag(sum(W2, 2));
        L = D - W2;
        
        all_idx = 1:num_centroids;
        l_idx = all_idx(sublabs ~= 0);
        u_idx = all_idx(sublabs == 0);

        %Yl = zeros(length(l_idx),2);
        %Yl = oneHot(Y_l);

        %Yu = inv(L(u_idx,u_idx)) * (W(u_idx,l_idx) * Yl) ;

        Wul = W2(u_idx,l_idx);
        Luu = L(u_idx,u_idx);
        %Vuu = ;
        %Yu = full(  );
        %u_idx
        %L
        %Luu
        Yu = inv(Luu) * (Wul * Yl) ;
        
        %compute the labels
        y = sublabs;
        
        [~, label_1_2] = max(Yu, [], 2);
        y(sublabs == 0) = 2*label_1_2 - 3;% +1 -1 labels

        fprintf('label %d confidence %.5f opposite %.5f time %d\n',y(find(centroids_to_nodes_map == t)),Yu(find(centroids_to_nodes_map(sublabs == 0) == t),1), Yu(find(centroids_to_nodes_map(sublabs == 0) == t),2),t);

        y = y(find(centroids_to_nodes_map == t));
       
        
end
