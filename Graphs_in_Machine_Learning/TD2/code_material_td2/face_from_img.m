function [ gray_face ] = face_from_img( im )
    gray_face = zeros(10);
    cc = cv.CascadeClassifier('haarcascade_frontalface_default.xml');
    box = cc.detect(im);
    top_face.area = 120*120;
    frame.width = size(im,2);
    frame.height = size(im,1);
    for z = 1:length(box)
        rect.x = box{z}(1);
        rect.y = box{z}(2);
        rect.width = box{z}(3);
        rect.height = box{z}(4);
        face_area = rect.width*rect.height;
        if (face_area > top_face.area)
            top_face.area = face_area;
            top_face.x = box{z}(1);
            top_face.y = box{z}(2);
            top_face.width = box{z}(3);
            top_face.height = box{z}(4);
            gray_im = cv.cvtColor(im, 'BGR2GRAY');
            gray_face = gray_im( top_face.y:top_face.y + top_face.height, top_face.x:top_face.x + top_face.width);
            fprintf('kept %i x %i\n', rect.width, rect.height)
        else
            fprintf('skipped %i x %i\n', rect.width, rect.height)
        end
        
    end

end

