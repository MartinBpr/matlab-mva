%function ossl_wrap(cap)
    cap = cv.VideoCapture('/Users/Martin/Downloads/video.avi')
    cc = cv.CascadeClassifier('haarcascade_frontalface_default.xml');

    EXTR_FRAME_SIZE = 96;

    max_num_centroids = 100;

    figure();
    f = imshow(cv.resize(cap.read(),[640,480]));
    
    if exist(['data_online.mat'], 'file')
       load(['data_online.mat']);
    else
        centroids = [];
        labels = [];
        for i = 1:10
            im = imread(sprintf('labels_pic/%s_%d.bmp','martin',i));
            fprintf('labels_pic/%s_%d.bmp\n','martin',i)
            gray_face = face_from_img(im);
            if(norm(double(gray_face)) > 0)
                centroids = [centroids;extract_face_features(gray_face)];
                labels = [labels;1];
            else
            end
        end
        for i = [1:4 6:10]
            im = imread(sprintf('labels_pic/%s_%d.bmp','charlotte',i));
            fprintf('labels_pic/%s_%d.bmp\n','charlotte',i)
            gray_face = face_from_img(im);
            if(norm(double(gray_face)) > 0)
                centroids = [centroids;extract_face_features(gray_face)];
                labels = [labels;-1];
            end
        end

        save('data_online', 'centroids', 'labels')
    end

    
    num_labels = size(labels,1);

    nodes_to_centroids_map = [1:num_labels];
    centroids_to_nodes_map = [1:num_labels];
    taboo = true(size(num_labels));
    R = 0;
    
    t = num_labels + 1;
    while t < 10000
        im = cv.resize(cap.read(),[640,480]);
        box = cc.detect(im);
        frame.width = size(im,2);
        frame.height = size(im,1);

        top_face.idx = 0;
        top_face.dst = Inf;
        for i = 1:length(box)
            rect.x = box{i}(1);
            rect.y = box{i}(2);
            rect.width = box{i}(3);
            rect.height = box{i}(4);
            %im = cv.rectangle(im,box{i});
			%face_dist = norm([rect.x + rect.width / 2 - frame.width / 2, rect.y + rect.height / 2 - frame.height / 2]);
			%if (face_dist < top_face.dst)
				%top_face.idx = i;
				%top_face.dst = face_dist;
                %top_face.x = box{i}(1);
                %top_face.y = box{i}(2);
                %top_face.width = box{i}(3);
                %top_face.height = box{i}(4);
            %end
            %fprintf('%f x %f\n', rect.width, rect.height)
            if (rect.width > 120) && (rect.height > 120)
                gray_im = cv.cvtColor(im, 'BGR2GRAY');
                gray_face = gray_im( rect.y:rect.y + rect.height, rect.x:rect.x + rect.width);
                gray_face = extract_face_features(gray_face);

                [centroids, nodes_to_centroids_map, centroids_to_nodes_map, taboo, R] = update_centroids(centroids, max_num_centroids, nodes_to_centroids_map, centroids_to_nodes_map, taboo, R, num_labels, gray_face, t);
                labels = [labels;0];
                y = compute_solution(t, centroids, nodes_to_centroids_map, centroids_to_nodes_map, labels);
                if y ==1
                    im = cv.rectangle(im,box{i},'Color',[255,0,0],'Thickness',2);
                else
                    im = cv.rectangle(im,box{i},'Color',[0,255,0],'Thickness',2);
                end

                t = t+1;
            end
            
            %if t==300
            %    figure(1)
            %    for i=1:size(centroids,1)
            %        subplot(10,10,i);
            %        h = imshow(reshape(centroids(i,:),EXTR_FRAME_SIZE,EXTR_FRAME_SIZE), 'InitialMag',100, 'Border','tight');
            %        title(num2str(i))
            %    end 
            %end

        end
        set(f,'CData', im);
        drawnow()
        pause(1/30)



    end
