function [ARI] = spectral_clustering(graph_type, graph_thresh)
% spectral_clustering
% a skeleton function to perform spectral clustering, needs to be completed
% graph_type, graph_thresh: in batch mode, control the graph construction,
%                           when missing simply plot the result
% ARI: return the quality of the clustering


%%%%%%%%%%%% the number of samples to generate
num_samples = 600;

%%%%%%%%%%%% the sample distribution function with the options necessary for the
%%%%%%%%%%%% distribuion

% choose wich question you are dealing with
question = 22;

if question == 21
    sample_dist = @blobs;
    dist_options = [2, 0.05, 0]; % blobs: number of blobs, variance of gaussian
    %                                    blob, surplus of samples in first blob
elseif question == 22
    sample_dist = @two_moons;
    dist_options = [1, 0.02]; % two moons: radius of the moons,
    %                                   variance of the moons
elseif question == 23;
    sample_dist = @point_and_circle;
    dist_options = [7, 3]; % point and circle: radius of the circle,
    %                           variance of the circle
end

normalize_version = true;

plot_results = 0;

if nargin < 1

    plot_results = 1;

    %%%%%%%%%%%% the type of the graph to build and the respective threshold

    if question == 21 || question == 22
        graph_type = 'eps';
        graph_thresh = 0.8; % the epsilon threshold`
        normalize_version = false;
        
    elseif question == 23
        graph_type = 'knn';
        graph_thresh = 7; % the number of neighbours for the graph
        normalize_version = true;
    end

end

%%%%%%%%%%%% similarity function
similarity_function = @exponential_euclidean;

%%%%%%%%%%%% similarity options
similarity_options = [0.5]; % exponential_euclidean: sigma

[X, Y] = get_samples(sample_dist, num_samples, dist_options);

%%%%%%%%%%%% automatically infer number of labels from samples
num_classes = length(unique(Y));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% use the build_similarity_graph function to build the graph W  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

W = build_similarity_graph(graph_type, graph_thresh, X, ...
    similarity_function, similarity_options);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% build the laplacian                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

D = diag(sum(W, 2));
L = D - W;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute eigenvectors                                      %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if normalize_version
    try
        [U, E] = eigs(L, D, 20, 'sm');
    catch err
        [U, E] = eig(L, D);
    end
else
    try
        [U, E] = eigs(L, 20, 'sm');
    catch err
        [U, E] = eig(L);
    end
end

ev = diag(E);


%% Remove 1 vector if it is present
[U, ev] = remove_all_ones_vector(U, ev);

[eigenvalues_sorted, reorder] = sort(ev);
%figure; bar(eigenvalues_sorted(1:10));
%eigenvalues_sorted(1:10)
eig_ind = choose_eigenvalues(eigenvalues_sorted);
U_kept = U(:, reorder(eig_ind));

if normalize_version
    for i = 1:size(U_kept,1)
        if norm(U_kept(i,:)) > 0
            U_kept(i,:) = U_kept(i,:) / norm(U_kept(i,:));
        end
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute the clustering assignment from the eigenvector    %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%kmeans on eigen vectors
label = kmeans(U_kept, 2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if plot_results
    plot_clustering_result(X,Y,W,label,kmeans(X,num_classes));
end

if nargout == 1
    ARI = ari(Y,label);
end

