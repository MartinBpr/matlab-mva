function [] = image_segmentation()
% image_segmentation

n= 50; %
num_samples = n*n;

%% Graph parameters
% the type of the graph to build and the respective threshold
graph_type = 'eps';
graph_thresh = 0.005;

% similarity function
similarity_function = @exponential_euclidean;

% similarity options
similarity_options = [100]; % exponential_euclidean: sigma


%% Choose options (described in 3.1)
normalize_version = true;
add_coordinates = true;
force_5_ev = false;
assimilate_grey_to_white = true;

% it makes no sense to remove grey awhile forcing to keep its eigen value
assert(~ (assimilate_grey_to_white && force_5_ev))

%% Import and treat image
X = imread('four_elements','bmp');

if add_coordinates
    values = zeros(n,n,5);
    values(:,:,1:3) = double(X);
    values(:,:,4) = repmat((1:n)',1,n); % add lines
    values(:,:,5) = repmat(1:n,n,1); % add columns
    values = reshape(values,num_samples,5);
else
    values = double(X);
    values = reshape(values,num_samples,3);
end

if assimilate_grey_to_white
    values(:,1:3) = values(:,1:3) - repmat(mean(values(:,1:3), 2), 1, 3);
end


W = build_similarity_graph(graph_type, graph_thresh, values, ...
    similarity_function, similarity_options);


%% build the laplacian                                          

D = diag(sum(W, 2));
L = D - W;


%% compute eigenvectors                                      

if normalize_version 
    [U, E] = eigs(L, D, 30, 'sm');
else
    [U, E] = eigs(L, 30, 'sm');
end

ev = diag(E);


%% Remove 1 vector if it is present
[U, ev] = remove_all_ones_vector(U, ev);

%% Sort and select eigen values
[eigenvalues_sorted, reorder] = sort(ev);

figure; scatter(1:13,eigenvalues_sorted(1:13));

if force_5_ev
    eig_ind = 1:5;
else
    eig_ind = choose_eigenvalues(eigenvalues_sorted);
end

U_kept = U(:, reorder(eig_ind));

% Normalize vectors over rows
if normalize_version
    for i = 1:size(U_kept,1)
        if norm(U_kept(i,:)) > 0
            U_kept(i,:) = U_kept(i,:) / norm(U_kept(i,:));
        end
    end
end


%% compute the clustering assignment from the eigenvectors    

%kmeans on eigen vectors
assert(length(eig_ind) < 30) % To ensure the gap was found
label = kmeans(U_kept, length(eig_ind) + 1);

%% Plot clustering
set(figure(), 'units', 'centimeters', 'pos', [0 0 20 10]);

subplot(1,2,1);

imagesc(reshape(X,50,50,3));

subplot(1,2,2);
imagesc(reshape(label,50,50));

end

