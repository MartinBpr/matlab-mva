function [] = how_to_choose_epsilon()%graph_type, graph_thresh, X, similarity_function, similarity_options)
% how_to_choose_epsilon
% a skeleton function to analyze the influence of the graph structure
% on the epsilon graph matrix, needs to be completed


%%%%%%%%%%%% the number of samples to generate
num_samples = 100;

%%%%%%%%%%%% the sample distribution function
sample_dist = @worst_case_blob;

%%%%%%%%%%%% the type of the graph to build
graph_type = 'eps';

%%%%%%%%%%% the options necessary for the distribution
dist_options = [30]; % read worst_case_blob.m to understand the meaning of
%                               the parameter

%%%%%%%%%%% similarity function
similarity_function = @exponential_euclidean;

%%%%%%%%%%% similarity options
similarity_options = [0.5]; % exponential_euclidean: sigma

[X, Y] = get_samples(sample_dist, num_samples, dist_options);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% use the similarity function and the min_span_tree function    %
% to build the minimum spanning tree min_tree                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


distances = similarity_function(X, similarity_options);
min_tree = min_span_tree(distances);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set graph_thresh to the minimum weight in min_tree            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tree_weights = distances .* min_tree;
graph_thresh = min(tree_weights(min_tree > 0));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% use the build_similarity_graph function to build the graph W  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

W = build_similarity_graph(graph_type, graph_thresh, X, similarity_function, similarity_options);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

graph_thresh
plot_graph_matrix(X,W);
