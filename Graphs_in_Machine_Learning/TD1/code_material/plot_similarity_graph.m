%nafis.fa3@gmail.com

function [] = plot_similarity_graph()
% plot_similarity_graph
% a skeleton function to analyze the construction of the graph similarity
% matrix, needs to be completed

%%%%%%%%%%% the number of samples to generate
num_samples = 500;

%%%%%%%%%%% the sample distribution function with the options necessary for
%%%%%%%%%%% the distribution

sample_dist = @blobs;
dist_options = [1, 1, 0]; % blobs: number of blobs, variance of gaussian
%                                    blob, surplus of samples in first blob

%sample_dist = @two_moons;
%dist_options = [1, 0.02]; % two moons: radius of the moons,
%                                   variance of the moons

%sample_dist = @point_and_circle;
%dist_options = [7, 3]; % point and circle: radius of the circle,
%                           variance of the circle

%%%%%%%%%%% the type of the graph to build and the respective threshold

%graph_type = 'knn';
%graph_thresh = 100; % the number of neighbours for the graph
%graph_thresh = 4; % the number of neighbours for the graph

graph_type = 'eps';
graph_thresh = 0.1; % the epsilon threshold

%%%%%%%%%%% similarity function
similarity_function = @exponential_euclidean;

%%%%%%%%%%% similarity options
similarity_options = [0.1]; % exponential_euclidean: sigma
%similarity_options = [100]; % exponential_euclidean: sigma

[X, Y] = get_samples(sample_dist, num_samples, dist_options);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% use the build_similarity_graph function to build the graph W  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


W = build_similarity_graph(graph_type, graph_thresh, X, ...
    similarity_function, similarity_options);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

plot_graph_matrix(X,W);
%title(['type' graph_type ', thres' num2str(graph_thresh) ', sigma' num2str(similarity_options(1))])

