function [eig_ind] = choose_eigenvalues(eigenvalues)
% choose_number_of_eigenvalues
% receives in input a sorted list of eigenvalues
% and returns the indexes of the one that will be used to
% chose the eigenvectors for the clustering.
% e.g. [1,2,3,5] 1st, 2nd, 3rd, and 5th smallest eigenvalues

zero_ev = eigenvalues(abs(eigenvalues) < 1e-10);

% if we have zero eigen values it means we have separated connected
% components, we use them for our spectral clustering
if ~ isempty(zero_ev)
   eig_ind = 1:length(zero_ev); 

% Otherwise we aim to find the bend by looking at the relative gaps observed
else
    relative_gaps = (eigenvalues(2:end) - eigenvalues(1:end-1)) ./ ...
        (eigenvalues(2:end) + eigenvalues(1:end-1));
    [~, ind] = max(relative_gaps(1:end));
    eig_ind = 1:(ind);
end

disp(['chosen eigen values ' num2str(eig_ind)])
end