function [] = find_the_bend()
% a skeleton function to analyze how the choice of number of eigenvectors
% influences the clustering
%

%%%%%%%%%%%%%the number of samples to generate
num_samples = 600;

%%%%%%%%%%%%%the sample distribution function
sample_dist = @blobs;

question = 27;

if question == 26
    dist_options = [4, 0.03, 0]; % blobs: number of blobs, variance of gaussian
    %                                    blob, surplus of samples in first blob 
elseif question == 27
    dist_options = [4, 0.20, 0]; % blobs: number of blobs, variance of gaussian
    %                                    blob, surplus of samples in first blob
end


graph_type = 'knn';
graph_thresh = 7; % the number of neighbours for the graph

%%%%%%%%%%%%%the options necessary for the distribution

%%%%%%%%%%%%%similarity function
similarity_function = @exponential_euclidean;

%%%%%%%%%%%%%similarity options
similarity_options = [0.5]; % exponential_euclidean: sigma


[X, Y] = get_samples(sample_dist, num_samples, dist_options);

%%%%%%%%%%%%% automatically infer number of labels from samples
num_classes = length(unique(Y));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% use the build_similarity_graph function to build the graph W  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

W = build_similarity_graph(graph_type, graph_thresh, X, ...
    similarity_function, similarity_options);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% build the laplacian                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

D = diag(sum(W, 2));
L = D - W;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute eigenvectors                                      %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[U, E] = eig(L);
ev = diag(E);

%% Remove 1 vector if it is present
to_keep_eigen_vectors =  ones(1, num_samples) > 0;
for i = 1:num_samples
    if is_colinear(U(:, i), ones(num_samples, 1))
        to_keep_eigen_vectors(i) = false;
    end
end

U = U(:,to_keep_eigen_vectors);
ev = ev(to_keep_eigen_vectors);

disp(['discarded vectors ' num2str(num_samples - length(ev))])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[eigenvalues_sorted,reorder] = sort(ev);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% choose number of eigenvectors to use                      %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

eig_ind = choose_eigenvalues(eigenvalues_sorted);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

U = U(:,reorder(eig_ind));

[label] = kmeans(U, num_classes,'EmptyAction','singleton','Start','uniform','Replicates',3);

plot_the_bend(X, Y, W, label, eigenvalues_sorted([eig_ind,(max(eig_ind)+1):max(eig_ind)+10]));

