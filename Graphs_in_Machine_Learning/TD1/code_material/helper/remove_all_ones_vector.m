function [ U ev ] = remove_all_ones_vector( U, ev )
%% Remove 1 vector if it is present
num_samples = size(U, 1);

to_keep_eigen_vectors =  ones(1, size(U,2)) > 0;
for i = 1:size(U,2)
    if is_colinear(U(:, i), ones(num_samples, 1))
        to_keep_eigen_vectors(i) = false;
    end
end

disp(['discarded vectors ' num2str(length(to_keep_eigen_vectors) - sum(to_keep_eigen_vectors))])

U = U(:,to_keep_eigen_vectors);
ev = ev(to_keep_eigen_vectors);

end

