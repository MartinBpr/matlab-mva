function [ test ] = is_colinear( v1, v2 )
% test : colinearity is true if Cauchy Schwarz inequality is an equality

left_term_CS = sum(v1.*v2)^2;
right_term_CS = sum(v1.*v1)*sum(v2.*v2);

test = (right_term_CS - left_term_CS < 1e-10);

end

