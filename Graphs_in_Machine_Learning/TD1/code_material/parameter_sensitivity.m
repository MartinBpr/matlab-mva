function [] = parameter_sensitivity()
% parameter_sensitivity
% a skeleton function to test spectral clustering
% sensitivity to parameter choice

graph_type = 'knn';
parameter_candidate = [2,3,4,5,6,7,8,9,10,13,20,35,50];

parameter_candidate = sort(parameter_candidate);
parameter_performance = zeros(length(parameter_candidate), 1);

for i = 1:length(parameter_candidate)
    parameter_performance(i) = spectral_clustering(graph_type, parameter_candidate(i));
end

subplot(1,2,1)
plot(parameter_candidate, parameter_performance);
ylabel('ARI','FontSize',15.4);
xlabel('number of neighbors','FontSize',17);
title('knn parameter sensitivity','FontSize',18);


graph_type = 'eps'
parameter_candidate = [(1:9)/10 (((5:9)/10) + 0.05)];

parameter_candidate = sort(parameter_candidate);
parameter_performance = zeros(length(parameter_candidate), 1);

for i = 1:length(parameter_candidate)
    parameter_performance(i) = spectral_clustering(graph_type, parameter_candidate(i));
end

subplot(1,2,2)
plot(parameter_candidate, parameter_performance);
ylabel('ARI','FontSize',15.4);
xlabel('value of epsilon','FontSize',17);
title('epsilon graph parameter sensitivity','FontSize',18);
end



