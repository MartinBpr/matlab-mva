function [W,distances] = build_similarity_graph(graph_type, graph_thresh, X, similarity_function, similarity_options)
%[W, distances] = build_similarity_graph(graph_type, graph_thresh, X, similarity_function, similarity_options)
% Computes the similarity matrix for a given dataset of samples.
% graph_type: knn or eps graph, as a string, controls the graph that
% the function will produce
% graph_thresh: controls the main parameter of the graph, the number
% of neighbours k for k-nn, and the threshold eps for epsilon graphs
% X: is an n x m matrix of m-dimensional samples
% similarity_function: is a function handle that will be applied to
% compute the sample distance
% similarity_options: is a vector containing options for the similarity
% function
% The return value W is an n x n graph matrix, while the distances
% matrix (if requested) contains the full matrix with all the distances


if nargin < 4
    error('build_similarity_graph: not enough arguments')
elseif nargin < 5
    similarity_options = [];
elseif nargin > 6
    error('build_similarity_graph: too many arguments')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% use similarity function to build full graph (distances)   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
distances = similarity_function(X, similarity_options);
assert(issymmetric(distances));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n = size(X,1);

A = zeros(n,n);

if strcmp(graph_type,'knn') == 1

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%% compute a k-nn graph from the distances            %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for i = 1:n
        [~, sortIndex] = sort(distances(i, :),'descend');
        
        % plus 1 to include the point itself and its k-nn
        A(i, sortIndex(1:(graph_thresh + 1))) = 1;
        % We add this to ensure our graph to be symetric
        A(sortIndex(1:(graph_thresh + 1)), i) = 1;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elseif strcmp(graph_type,'eps') == 1

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%% compute an epsilon graph from the distances        %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    A = (distances >= graph_thresh);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

else

    error('build_similarity_graph: not a valid graph type')

end

% Let's verify A is symetric
assert(norm(A - A.') == 0);

% Remove the diagonal
A(logical(eye(size(A)))) = 0;

%Compute Weights matrix
W = A.*distances;
%Let's verify W is symetric
assert(issymmetric(W));


end