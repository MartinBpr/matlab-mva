function [ p_Zt, p_Zt_Zt_plus_1, llh ] = E_step(u, mu, sigmas, A, pis)

[T, ~] = size(u);
[K, ~] = size(mu);

log_alpha = compute_log_alpha(u, mu, sigmas, A, pis);
log_beta = compute_log_beta(u, mu, sigmas, A);

p_Zt = zeros(T, K);
for t = 1:T
    log_al_be = log_alpha(t,:) + log_beta(t,:);
    ai = log_al_be;
    b = max(ai);
    p_Zt(t,:) = exp( log_al_be - b - log(sum(exp(ai - b) )));
end


p_Zt_Zt_plus_1 = zeros(T-1, K, K);
for t = 1:(T-1)
    log_al_be = log_alpha(t,:) + log_beta(t,:);
    ai = log_al_be;
    b = max(ai);
    log_p =  b + log(sum(exp(ai - b) ));
    
    u_t_plus_1 = u(t+1,:);
    for q = 1:K
        for q_t_plus_one = 1:K
            log_p_q_t_plus_one_knowing_q_t = log(A(q,q_t_plus_one));

            mu_q_plus_one = mu(q_t_plus_one,:);
            sigma_q_plus_one = sigmas{q_t_plus_one};
            log_p_u_t_plus_1_knowing_q_t_plus_1 = log_normale_density(u_t_plus_1,...
                mu_q_plus_one, sigma_q_plus_one);

            p_Zt_Zt_plus_1(t, q, q_t_plus_one) = exp (- log_p + log_alpha(t, q) + log_beta(t+1, q_t_plus_one) + ...
                log_p_q_t_plus_one_knowing_q_t + log_p_u_t_plus_1_knowing_q_t_plus_1);
        end
    end
end

b = max(log_alpha(T,:));
llh = b + log(sum(exp(log_alpha(T,:) - b)));

end

