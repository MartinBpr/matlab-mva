function [ log_beta ] = compute_log_beta(u, mu, sigmas, A)

[T, ~] = size(u);
[K, ~] = size(mu);

log_beta = zeros(T, K);
log_beta(T,:) = log(ones(K,1));

for t = (T-1):-1:1
    u_t_plus_1 = u(t+1,:);
    for q = 1:K
        ai = zeros(K,1);
        for q_t_plus_one = 1:K
            log_p_q_t_plus_one_knowing_q_t = log(A(q,q_t_plus_one));
            
            mu_q = mu(q_t_plus_one,:);
            sigma_q = sigmas{q_t_plus_one};
            log_p_u_t_plus_1_knowing_q_t_plus_1 = log_normale_density(u_t_plus_1, mu_q, sigma_q);
            
            ai(q_t_plus_one) = log_p_q_t_plus_one_knowing_q_t + log_p_u_t_plus_1_knowing_q_t_plus_1 ...
                + log_beta(t+1, q_t_plus_one);
            
        end
        b = max(ai);
        log_beta(t, q) = b + log(sum(exp(ai-b)));
    end
end

end

