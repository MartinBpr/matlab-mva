function [ log_alpha ] = compute_log_alpha(u, mu, sigmas, A, pi_init)

[T, ~] = size(u);
[K, ~] = size(mu);

log_alpha = zeros(T, K);

% initialisation
for q = 1:K
    mu_q = mu(q,:);
    sigma_q = sigmas{q};
    log_p_u_t_knowing_q_t = log_normale_density(u(1,:), mu_q, sigma_q);
    log_alpha(1,q) = log_p_u_t_knowing_q_t + log(pi_init(q));
end


for t = 2:T
    u_t = u(t,:);
    for q = 1:K
        mu_q = mu(q,:);
        sigma_q = sigmas{q};
        log_p_u_t_knowing_q_t = log_normale_density(u_t, mu_q, sigma_q);
        
        log_p_q_t_knowing_q_t_minus_1 = log(A(:,q));
        
        ai = log_p_q_t_knowing_q_t_minus_1 + log_alpha(t-1,:)';
        b = max(ai);
                              
        log_alpha(t, q) = log_p_u_t_knowing_q_t + ...
            b + log(sum(exp(ai-b)));
    end
end

end

