function [ viterbi_path ] = viterbi( u, A, mu, sigmas, pis )

[T, ~] = size(u);
[K, ~] = size(mu);

log_T1 = zeros(T, K);
T2 = zeros(T, K);


for q = 1:K
    mu_q = mu(q,:);
    sigma_q = sigmas{q};

    log_p_u_t_knowing_q_t = log_normale_density(u(1,:), mu_q, sigma_q);
    log_T1(1,q) = pis(q) + log_p_u_t_knowing_q_t;
end


for t=2:T
    for q = 1:K
        mu_q = mu(q,:);
        sigma_q = sigmas{q};

        log_p_u_t_knowing_q_t = log_normale_density(u(t,:), mu_q, sigma_q);
        
        [log_t1, t2] = max(log_T1(t-1, :) + log(A(:, q))' + log_p_u_t_knowing_q_t);
        log_T1(t,q) = log_t1;
        T2(t,q) = t2;
    end 
end

viterbi_path = zeros(1,T);
[~, last_path] = max(log_T1(T,:));
viterbi_path(T) = last_path;
for t=T:-1:2
    viterbi_path(t - 1) = T2(t, viterbi_path(t));
end


end

