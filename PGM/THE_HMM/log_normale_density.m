function [ log_px ] = log_normale_density( x, mu, sigma )
    d = length(x);
    xc = x - mu;
    log_px = - .5 * sum( (xc / sigma) .* xc , 2 ) - .5 * sum( log( eig( sigma) ) )  - .5 * d * log(2*pi);
end

