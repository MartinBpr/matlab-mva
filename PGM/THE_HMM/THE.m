%%
%%%%% INITIALISATION %%%%%

clear all
% load data
u = textread('EMGaussienne.data');
utest = textread('EMGaussienne.test');
[ T d ] = size(u);
[ T_test d ] = size(utest);
K = 4;

% random seed (make sure that everything is reproducible)
seed=1;
rand('state',seed);
randn('state',seed);

% variables sigmas and mus load from Homework 2
load('sigmas_EM.mat')
load('mus_EM.mat')
A = diag(ones(1,4)*(0.5-1/6))+1/6;
pis = ones(1,K) / K;

%%
% Question 2

[ p_Zt, ~ , ~] = E_step(u, mu, sigmas, A, pis);

figure;

n = 100;
for q=1:K
    subplot(K,1,q)
    bar(p_Zt(1:n,q))
    xlim([0 101])
    ylabel('probabilit?')
    v = axis;
    handle=title(['?tat ' num2str(q)], 'FontSize', 18);
    set(handle,'Position',[-11 v(4)*.4 0]);
end
xlabel('it?ration')

savefig('Question2')

%%
% Question 4

%%%%%% EM ALGORITHM %%%%%%

steps = 100;
loglik_train = zeros(1, steps);
loglik_test = zeros(1, steps);
tolerance = 1e-10;

for ite = 1:steps
    %E_step
    [ p_Zt, p_Zt_Zt_plus_1, llh_train ] = E_step(u, mu, sigmas, A, pis);
    
    llh_test = loglikelihood(utest, mu, sigmas, A, pis);
    
    disp([num2str(ite) ' ' num2str(llh_train)]);
    
    loglik_train(ite) = llh_train;
    loglik_test(ite) = llh_test;
    
    if ite > 1 && llh_train < loglik_train(ite-1) - tolerance, error('the distortion is going up!'); end % important for debugging
    if ite > 1 && llh_train < loglik_train(ite-1) + tolerance
        steps = ite;
        loglik_train = loglik_train(1:steps);
        loglik_test = loglik_test(1:steps);
        break; 
    end

    % M-step:
    oldmu = mu;
    for k=1:K
        mu(k,:) = sum( repmat( p_Zt(:,k),1 ,d) .* u ) / sum( p_Zt(:,k) );
        pis(k) = p_Zt(1,k);
        temp = u - repmat( oldmu(k,:), T , 1 );
        sigmas{k} = 1 / sum( p_Zt(:,k) ) * ( temp' * ( repmat( p_Zt(:,k) ,1,d) .* temp ) );
        for l=1:K
           A(k,l) = sum(p_Zt_Zt_plus_1(:, k, l))/sum(sum(p_Zt_Zt_plus_1(:, k, :)));
        end
    end
end

% print parameters
disp(['pis : ' mat2str(pis)]);
disp(['A : ' mat2str(A)]);

for k = 1:4
    disp(['mu' num2str(k) ' : ' mat2str(mu(k,:))]);
    disp(['sigma' num2str(k) ' : ' mat2str(sigmas{k})]);
end

%%
% Question 5

figure;
subplot(1,1,1);
plot(1:steps, loglik_train, '-o', 1:steps, loglik_test, '-o');
xlim([1 min(steps,10)]);
set(gca, 'FontSize',14)
title('Log vraisemblance', 'FontSize', 18);
h_legend = legend('?chantillon d''entrainement', '?chantillon de test', 'Location', 'SouthEast');
set(h_legend,'FontSize',16);

savefig('Question5')

disp(['Last llh on train ' num2str(loglik_train(steps))]);
disp(['Last llh on test  ' num2str(loglik_test(steps))]);

%%
% Question 7

%%%%%% VITERBI %%%%%

viterbi_path = viterbi( u, A, mu, sigmas, pis );

figure;
subplot(1,1,1);
colors = { 'k' 'r' 'g' 'b' };
c_colors = { 'r' 'k' 'k' 'r' };
hold on

% first time to have them in the legend
for k=1:K
    plot( mu(k,1),mu(k,2),sprintf('%s%s',c_colors{k},'s'), 'MarkerFaceColor', colors{k}, ...
        'MarkerSize', 12);
end

R2 = my_chi2inv(.9,2);

for k=1:K
    ind = find(viterbi_path==k);
    plot( u(ind,1),u(ind,2),sprintf('%s%s',colors{k},'x'));
    draw_ellipse_color( inv(sigmas{k}) , - sigmas{k} \ ( mu(k,:)' ), .5 * mu(k,:) * ( sigmas{k} \ ( mu(k,:)' ) )  - R2/2 ,colors{k})
end

viterbi_path = viterbi( utest, A, mu, sigmas, pis );
for k=1:K
    ind = find(viterbi_path==k);
    plot( utest(ind,1),utest(ind,2),sprintf('%s%s',colors{k},'o'));
end

% second time to have them over the other points...
for k=1:K
    plot( mu(k,1),mu(k,2),sprintf('%s%s',c_colors{k},'s'), 'MarkerFaceColor', colors{k}, ...
        'MarkerSize', 12);
end
hold off

title('Viterbi path allocation' ,'FontSize',18);
h_legend = legend('q1', 'q2', 'q3', 'q4', 'Location', 'NorthWest');
set(h_legend,'FontSize',14);

savefig('Question7')


%%
% Question 8


[ p_Zt, p_Zt_Zt_plus_1, ~ ] = E_step(utest, mu, sigmas, A, pis);

figure;
n = 100;
for q=1:K
    subplot(K,1,q)
    bar(p_Zt(1:n,q))
    xlim([0 101])
    ylabel('probabilit?')
    v = axis;
    handle=title(['?tat ' num2str(q)], 'FontSize', 18);
    set(handle,'Position',[-11 v(4)*.4 0]);
end
xlabel('it?ration')


savefig('Question8')

%% 
% Question 9
[~, margin_path] = max(p_Zt, [], 2);


figure;
subplot(1,1,1);
colors = { 'k' 'r' 'g' 'b' };
markers = { 'o' 's' '+' 'x' };
for k=1:K
    ind = find(margin_path(1:n)==k);
    plot( ind,ones(1, length(ind)),sprintf('%s%s',colors{k},markers{k}));
    hold on
end

%%
% Question 10
viterbi_path = viterbi( utest, A, mu, sigmas, pis );

colors = { 'k' 'r' 'g' 'b' };
markers = { 'o' 's' '+' 'x' };
for k=1:K
    ind = find(viterbi_path(1:n)==k);
    plot( ind,zeros(1, length(ind)),sprintf('%s%s',colors{k},markers{k}));
    hold on
end

ylim([-2.5,3.5]);
ax = gca;
ax.YTick = [0 1];
ax.YTickLabel = {'marginal probability','viterbi path'};
set(ax, 'FontSize',18)
xlim([0 109])

legend('q1', 'q2', 'q3', 'q4', 'Location', 'East');
savefig('Questions9-10')

