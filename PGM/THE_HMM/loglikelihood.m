function [ llh ] = loglikelihood(u, mu, sigmas, A, pis)
[T, ~] = size(u);

log_alpha = compute_log_alpha(u, mu, sigmas, A, pis);

b = max(log_alpha(T,:));
llh = b + log(sum(exp(log_alpha(T,:) - b)));
end