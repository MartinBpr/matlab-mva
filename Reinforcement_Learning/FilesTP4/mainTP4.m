%% Creation of the features

d = 20;% number of features
thetasQ  = rand_featureQ(d); 
%autre choix possible que le hasrard c'est une equirepartition des centres
%dans l'espace
% matrix thetasQ represents the feature space chosen 
% each line of this matrix is a vector that represents a feature  

% discount parameter
gamma=0.8;

%% How to visualize a linear combination of features?
%{
subplot(1,2,1)

alpha=ones(1,d);
Q=createQ(alpha,thetasQ);
V = @(s) Q(s,0);
plotf(V) 

subplot(1,2,2)
V = @(s) max(arrayfun(@(a)Q(s,a),[-1,0,1]));
plotf(V)
%}

T = 1000
n = 500


tic
alpha = fitted_q( thetasQ, T, n )
Q=createQ(alpha,thetasQ);
toc
V = @(s) max(arrayfun(@(a)Q(s,a),[-1,0,1]));
plotf(V)



alpha = ones(1,d)/d;
tic
alpha = LSTD( thetasQ, 1, n, 0.9, alpha)
Q=createQ(alpha,thetasQ);
toc
V = @(s) max(arrayfun(@(a)Q(s,a),[-1,0,1]));
%V = @(s) Q(s,0);
plotf(V)
