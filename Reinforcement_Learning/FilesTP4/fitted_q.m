function [alpha] = fitted_q( thetasQ, T, n)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    % apply bellman operator : Q_k+1 = T Q_k
    % we approximate T Q in space e (only weigths need to be stored)
    % Q_k+1(s,a) = sum_i=1^d alpha_i phi_i(s,a)
    % simulate n transitions : s_i and a_i at random
    % and observe y_i, r_i from simulator(s_i, a_i)
    % We get Zi and estimator of TQ = r_i + gamma * max_b Q_k(y_i,b) 
    % We get lots of couples : ((s_i, a_i), Z_i)
    % Q_k+1 = argmin_{Q\in e} 1/n * sum_i=1^n (Q(s_i, a_i) - Z_i)??
    %       = argmin_{alpha \in R^d} 1/n * sum_i=1^n (alpha' * X_i - Z_i)??
    % with X_i = [phi_1(s_i,a_i), ... , phi_d(s_i,a_i)]
    % alpha_k+1 = (X' X)^(-1) X' Z where X = [X_1', ..., X_n'] \in M_nd

[d, ~] = size(thetasQ);
alpha = ones(1,d)/d;
gamma = 0.9;

for k=1:T
    Q = createQ(alpha,thetasQ);

    %simulate n transitions
    x = rand(n,1)*(1.8) - 1.2;
    v = rand(n,1)*(0.14) - 0.07;
    
    s = zeros(n,2);
    s(:,1) = x;
    s(:,2) = v;
    
    a = randi(3,n,1) - 2;
    
    y = zeros(n,2);
    r = zeros(n,1);
    Z = zeros(n,1);
    X = zeros(n,d);
    for i=1:n
        [y(i,:), r(i)] = simulator(s(i,:), a(i));
        Z(i) = r(i) + gamma * max(arrayfun(@(a)Q(y(i,:) ,a),[-1,0,1]));
        for j=1:d
            X(i,j) = phiQ(s(i,:), a(i), thetasQ(j,:));
        end
    end
  
    alpha = (X'*X + 1e-4*eye(d))\X'*Z;
    alpha = alpha';
    %alpha
    k
end

end

