function [ sn, rec ] = simulator( s,a )
sn = zeros(1,2);

x_t = s(1);
v_t = s(2);
e = 0;

rec = -1;
if (x_t < 0.6)
    sn(1) = max( min( x_t + v_t, 0.6) , -1.2);
    sn(2) = max( min( v_t + e + 0.001*a - 0.0025*cos(3*x_t), 0.07) , -0.07);
else
    sn(1) = 0.6;
    sn(2) = 0;
end


if (sn(1) == 0.6) 
    rec = 0; % +inf?
end

end

