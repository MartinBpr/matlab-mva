%%%%%
% LSTD algorithm
%%%%%
function [alphaEnd] = LSTD(thetasQ, nbIter, nbSample, gamma, alpha)
% The function approximates the optimal Q-function and plots
% the associated Value function

    % Initializations
    nbFeatures = size(thetasQ, 1);
    
    
    alphaEnd = alpha;%ones(1,nbFeatures)/nbFeatures;
    
    actionChosen = zeros(nbSample, 1);
    
    
    X = zeros(nbSample + 1, 2);
    actionPossib = [-1; 0; 1]; 
    R = zeros(nbSample, 1);
    
    d = zeros(nbSample, nbFeatures);
    
    % Loop over the number of iterations
    dPast = zeros(nbSample, nbFeatures);
    for k = 1:nbIter
        
        % Current Q-function being given alpha
        Qk = createQ(alphaEnd, thetasQ);
        
        % Choice of the best action to take and taking of this action
        % First step
        [~, aChoice] = max([Qk(X(1, :), -1), Qk(X(1, :), 1), Qk(X(1, :), 0)]);
        actionChosen(1) = actionPossib(aChoice);
        [X(2, :), R(1)] = simulator(X(1, :), actionChosen(1));
        % Other steps in the trajectory
        for i = 2:nbSample
            
            [~, aChoice] = max([Qk(X(i, :), -1), Qk(X(i, :), 1), Qk(X(i, :), 0)]);
            actionChosen(i) = actionPossib(aChoice);
            [X(i+1, :), R(i)] = simulator(X(i, :), actionChosen(i));
            for j = 1:nbFeatures
                dPast(i, j) = phiQ(X(i - 1, :), actionChosen(i - 1), thetasQ(j, :));
                d(i,j) = phiQ(X(i, :), actionChosen(i), thetasQ(j,:));
            end
            
        end
        
        A = dPast' * (dPast - gamma * d) / nbSample;
        b = dPast' * R / nbSample;
        
        % Alpha at the en dof the iteration
        alphaEnd = A \ b;
        k
    end
    
    % Plot of the Value function
    %hold on;
    %V = @(s) Qk(s,0);
    %plotf(V) 
    %hold off;
end

