format compact;

s = gen_setting1();

M2 = zeros(s.K,s.K);
M3 = zeros(s.K,s.K,s.K);

J = 100;
n = 10000;
btheta = discreternd(s.rho, J);
min_t = 100;

for j=1:J
    best_arm = unidrnd(s.K-2);
    samples = {};
    n_remain = n;
    for i=1:s.K
        pulls = 0;
        if (i == best_arm)
            pulls = floor(min_t+n/2+unidrnd(floor(n/2)));
            samples{i} = (rand(pulls,1) < s.mu(btheta(j), i));
        else
            pulls = min_t+unidrnd(ceil(max(n_remain, 1)/(2*s.K)));
            samples{i} = (rand(pulls,1) < s.mu(btheta(j), i));
        end
        %samples{i} = samples{i}(1:min_t);
        n_remain = n_remain - pulls;
    end
    [hrho, model_hmu, rho_conf, model_conf, M2, M3] = exec_mom(s, samples, M2, M3, j);
    disp(model_conf);
end

% disp('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
% all_samples = {};
% for j=1:J
%     best_arm = unidrnd(s.K-2);
%     samples = {};
%     n_remain = n;
%     for i=1:s.K
%         pulls = 0;
%         if (i == best_arm)
%             pulls = floor(min_t+n/2+unidrnd(floor(n/2)));
%             all_samples{j,i} = (rand(pulls,1) < s.mu(btheta(j), i));
%         else
%             pulls = min_t+unidrnd(ceil(max(n_remain, 1)/(2*s.K)));
%             all_samples{j,i} = (rand(pulls,1) < s.mu(btheta(j), i));
%         end
%         %samples{i} = samples{i}(1:min_t);
%         n_remain = n_remain - pulls;
%     end
%     %all_samples{j} = samples;
%     [hrho, model_hmu, rho_conf, model_conf] = exec_mom_full(s, all_samples);
%     disp(model_conf);
% end