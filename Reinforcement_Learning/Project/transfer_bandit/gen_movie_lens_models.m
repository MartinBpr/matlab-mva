function models = gen_movie_lens_models(n_arms, n_models)

%n_arms = 5
%n_models = 3

%%import data
% load data
data = importdata('../ml-100k/u.data');
%data = importdata('/home/bomp/git/matlab-mva/Reinforcement_Learning/Project/ml-1m/u.data');

data = mat2dataset(data);
data = set(data,'VarNames',{'user','movie','rate','ts'});

% remove timestamp
data = data(:,{'user','movie','rate'});

user_stats = grpstats(data, 'user');
movie_stats = grpstats(data, 'movie');

%keep only the n most rated movies
n_movies = length(movie_stats);
n_selected_movies = n_arms;

sorted_movies = sortrows(movie_stats(:,'GroupCount'),1);
selected_movies = sorted_movies((n_movies-n_selected_movies + 1):n_movies,:);
selected_movies = cellfun(@str2num,selected_movies.Properties.ObsNames);

ds = data(ismember(data.movie,selected_movies),:);
length(ds);
ds_user_stats = grpstats(ds, 'user');

%%Create groups
%run k-means on user who have rated all films
all_rater_users = ds_user_stats(ds_user_stats.GroupCount==n_selected_movies,:).user; %size 135 (among 881)
ds_all_rater_users = ds(ismember(ds.user,all_rater_users),:);

%The matrix for kmeans
X = zeros(length(all_rater_users),n_selected_movies);

%to get the corresponding index in X for each selected movie
index_movies = containers.Map;
for i = 1:n_selected_movies 
    index_movies(num2str(selected_movies(i))) = i;
end

%to get the corresponding index in X for each all rater user
index_users = containers.Map;
for i = 1:length(all_rater_users)
    index_users(num2str(all_rater_users(i))) = i;
end

%fill the matrix
for i = 1:size(ds_all_rater_users,1)
    row = ds_all_rater_users(i,:);
    user_index = index_users(num2str(row.user));    
    movie_index = index_movies(num2str(row.movie));
    rate = row.rate;
    X(user_index, movie_index) = rate;
end

n_centers = n_models;
[idx, C] = kmeans(X, n_models, 'Replicates', 100);
%In order to always have the same center at the same coordinate
nb_all_rater_users_1 = sum(idx == 1)
nb_all_rater_users_2 = sum(idx == 2)
nb_all_rater_users_3 = sum(idx == 3)
nb_all_rater_users_4 = sum(idx == 4)


%%Find group for all users
%The matrix of rates
n_tot_users = length(grpstats(ds, 'user'));
X = zeros(n_tot_users,n_selected_movies);

%to get the corresponding index in X for each all rater user

% keep only users who have rated at least one movie
selected_movies_raters = ds_user_stats(ds_user_stats.GroupCount >= 1,:).user; %size 881 (among 943)
ds_selected_movies_raters = ds(ismember(ds.user,selected_movies_raters),:);

index_users = containers.Map;
for i = 1:length(selected_movies_raters)
    index_users(num2str(selected_movies_raters(i))) = i;
end

%fill the matrix
for i = 1:size(ds_selected_movies_raters,1)
    row = ds_selected_movies_raters(i,:);
    user_index = index_users(num2str(row.user));    
    movie_index = index_movies(num2str(row.movie));
    rate = row.rate;
    X(user_index, movie_index) = rate;
end

%affect user to closest center in his space
affectations = zeros(1,n_tot_users);
for i = 1:n_tot_users
    user = X(i,:);
    non_zeros = find(user);
    substract = user(ones(size(C,1),1),non_zeros) - C(:,non_zeros);
    distance = diag(sqrt(substract*substract'));
    [~,index_min] = min(distance);
    affectations(i) = index_min;
end

nb_users_1 = sum(affectations == 1)
nb_users_2 = sum(affectations == 2)
nb_users_3 = sum(affectations == 3)
nb_users_3 = sum(affectations == 4)

%create models with the clusters of his users
models = {};
for m = 1:n_models
    cluster_m = X(affectations==m,:);
    models{m} = model(cluster_m);
end

%simulation of 100 pulls on arm 2
%for t =1:100
%    arm = randi(n_arms);
%    models{2}.play(arm)
%end

