algs=(4)
n_values=(1000 2000 5000 10000 15000)
J_values=(1000 2000 5000)
n_nodes=50

for k in "${J_values[@]}"
do
	for j in "${algs[@]}"
	do
		for i in "${n_values[@]}"
		do
			echo "Execute experiments alg="$j" n="$i" J="$k
		
			launch_file="launch_transfer_set2_alg"$j"_n"$i"_J"$k".sh"
			taktuk_file="taktuk_transfer_set2_alg"$j"_n"$i"_J"$k".sh"
			job_name="a"$j"-n"$i"-J"$k
		
			echo "taktuk --connector /usr/bin/oarsh -f \$OAR_FILE_NODES broadcast exec [ ./"$launch_file" ]" > $taktuk_file
			chmod a+x $taktuk_file
			oarsub -t besteffort -n $job_name -l core=$n_nodes,walltime=10:00:00 /home/alazaric/$taktuk_file
	#		oarsub -n $job_name -l core=$n_nodes,walltime=20:00:00 /home/alazaric/$taktuk_file
		done
	done
done
