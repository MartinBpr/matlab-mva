function [M2,M3]=estMomRec(data,M2P, M3P, C,N)
%M1=zeros(C.K,1);
M2=zeros(C.K,C.K);
M3=zeros(C.K,C.K,C.K);
%for i=1:N
%M1=M1+data(i).tri(:,1);
M12 = data(:,1)*data(:,2)';
M13 = data(:,1)*data(:,3)';
M23 = data(:,2)*data(:,3)';
MC=M12+M13+M23;
M2=M2+MC+MC';
%vM2 = M2+data(i).tri(:,2)*data(i).tri(:,1)';
%M2 = M2+data(i).tri(:,3)*data(i).tri(:,1)';
%M2 = M2+data(i).tri(:,3)*data(i).tri(:,2)';
M2Rep1=repmat(M12,[1,1,C.K]); 
M3Rep1=repmat(reshape(data(:,3),[1,1,C.K]),[C.K,C.K,1]);
M2Rep2=repmat(M13,[1,1,C.K]); 
M3Rep2=repmat(reshape(data(:,2),[1,1,C.K]),[C.K,C.K,1]);
M2Rep3=repmat(M23,[1,1,C.K]); 
M3Rep3=repmat(reshape(data(:,1),[1,1,C.K]),[C.K,C.K,1]);
M2Rep4=repmat(M23',[1,1,C.K]); 
%M3Rep4=repmat(reshape(data(i).tri(:,1),[1,1,C.K]),[C.K,C.K,1]);
M2Rep5=repmat(M13',[1,1,C.K]); 
%M3Rep5=repmat(reshape(data(i).tri(:,2),[1,1,C.K]),[C.K,C.K,1]);
M2Rep6=repmat(M12',[1,1,C.K]); 
%M3Rep6=repmat(reshape(data(i).tri(:,3),[1,1,C.K]),[C.K,C.K,1]);
M3= M3+M2Rep1.*M3Rep1;
M3= M3+M2Rep2.*M3Rep2;
M3= M3+M2Rep3.*M3Rep3;
M3= M3+M2Rep4.*M3Rep3;
M3= M3+M2Rep5.*M3Rep2;
M3= M3+M2Rep6.*M3Rep1;

alpha=1/N;
%M1=M1/N;
M2=(1-alpha)*M2P+alpha*(M2+M2')/12;
M3=(1-alpha)*M3P+alpha*M3/6;
%M31=M3;
%M32=M3;
%M33=M3;
%M3=M3;

%for i=1:C.K
%M3(:,:,i)=reshape((reshape(M3(:,:,i),C.K,C.K)+reshape(M3(:,:,i),C.K,C.K)')/2,C.K,C.K,1);
%end;

%for i=1:C.K
%M3(:,i,:)=reshape((reshape(M3(:,i,:),C.K,C.K)+reshape(M3(:,i,:),C.K,C.K)')/2,C.K,1,C.K);
%end;


%for i=1:C.K
%M3(i,:,:)=reshape((reshape(M3(i,:,:),C.K,C.K)+(reshape(M3(i,:,:),C.K,C.K))')/2,1,C.K,C.K);
%end;

%M3=(M31+M32+M33)/3;


return;