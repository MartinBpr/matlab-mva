

%%%% muhat estimated average reward
%%%%  rhohat estimated model
%%%% Here the rewards are generated according to some gaussian distribution 
%%%% (in function SampGen) you can replace it with the rewards of your
%%%% choice just comment everything between 17 and 47 (except the for loop)
%%%% and put your codes bandit samples

rng(1000);
C.K=10;
C.m=3;
C.L=200;
C.L1=200;
sampNum=8;
NVect=logspace(1,sampNum/2,sampNum);
errMu=zeros(1,sampNum);
errRho=zeros(1,sampNum);
%N=10;
rho=rand(C.m,1);
C.rho=rho/sum(rho);

while min(C.rho)<1/(2*C.m)
rho=rand(C.m,1);
C.rho=rho/sum(rho);
end;

eps=0.5;
C.sig=0.5;
C.mu=rand(C.K,C.m);


while min(svd(C.mu))<eps
    C.mu=2*rand(C.K,C.m)-1;
end;


for c=1:sampNum
    
    
    data=SampGen(C,NVect(c));
    
    
    %%%%% data is a structure array with   NVect(c) (number of tasks) entries, 
    %%%%%%each entry is a 3\times K matrix consists of the first three samples for each arm 
    
    [muhat,rhohat,err1,err2]=MOMMainFunc(data,C.K,C.m,C.mu,C.rho);
    errMu(c)=err1;
    errRho(c)=err2;
    
end;


return;








