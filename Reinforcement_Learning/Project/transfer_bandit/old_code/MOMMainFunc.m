function [ rhohat, muhat , errMu, errRho ] = MOMMainFunc( dataC, K, m, mumat, rho )



%Inputs:

%%%%% dataC is a N*K cell array where N is   the number of tasks
%%%%% (so far ) and K is the number of arms
%%%each entry of the cell is a vector consisits of all the samples for each arm and
%%%each task. 




%%% K  number of arms

%%% m number of models

%%% rho the true distributions on the models

%%% mumat the true average rewards for all the models and all the arms (We
%%% need these two (mu, rho) to artificially make the confidence intervals since
%%% we do not yet have thecomplete theoretical bounds )



%Outputs:

%%%%rhohat: Emprirical rho

%%%% muhat: Empirical mu for all the models and arms it is a true mpirical
%%%% estimation up to a permutation in m

%%% errMu maximum error per arm per model for the estimated average reward

%%% errRho maximum error per model for the esimated rho



C.K=K;
C.m=m;
C.L=200;
C.L1=200;
C.mu=mumat;
C.rho=rho;
%sampNum=8;
%NVect=logspace(1,sampNum/2,sampNum);
errMu=0;
errRho=0;
N=size(dataC,1);


for i=1:N
    data(i).tri=zeros(K,3);
    for j=1:K
        data(i).tri(j,:)=dataC{i,j}(1:3);
    end
end;

[M1,M2,M3]=estMom(data,C,N);


[WM,W,D]=whiten(M2,M3,C);
[v,lambda]=eigDecomp(WM,C);


muhat=ones(C.K,1)*lambda'.*(pinv(W')*v);
rhohat=1./(lambda.^2);

for j=1:C.m
    
    distMin=norm(muhat+C.mu);
    distMinRho=1;
    
    for k=1:C.m
        distMin=min(max(abs((muhat(:,j)-C.mu(:,k)))),distMin);
        distMinRho=min(abs(rhohat(j)-C.rho(k)),distMinRho);
    end;
    errMu=max(errMu,distMin);
    errRho=max(errRho,distMinRho);
    
end;

return;




