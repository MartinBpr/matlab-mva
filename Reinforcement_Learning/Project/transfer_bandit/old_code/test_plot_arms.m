K = 4;
M = 3;

mu = rand(M,K);
btheta = 3;
hmu = mu(btheta,:)+0.05.*rand(1,K);
pulls = [10 50 10 20];
arm_conf = sqrt(1./pulls);

hmodel_mu = mu + 0.02.*rand(M,K);
episodes = 40;
model_conf = 1/sqrt(episodes);

active_models = compute_uncertain_active_set(M, K, hmodel_mu, model_conf, hmu', arm_conf');

fig = figure('XVisual','');
plot_arms(fig, K, M, mu, btheta, hmu, pulls, arm_conf, hmodel_mu, episodes, model_conf, active_models)