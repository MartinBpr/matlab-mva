function pulls = exec_run(mu, optmu, Gamma, invSqrGamma, Delta, rho, theta, K, M, n, alg)

pulls = zeros(K, 1);
cumulX = zeros(K, 1);
hmu = zeros(K, 1);

c = 1.0*sqrt(log(n)/2);
c2 = 4*log(n);

for t=1:n
    conf = c*sqrt(1./pulls);

    if (alg == 1) 
    %% UCB
        B = hmu + conf;
        [best_value best_arm] = max(B);
    
    elseif (alg == 2) 
    %% MUCB
        % compute the active set
        Theta_t = compute_active_set(mu, M, K, hmu, conf);
        
        % compute the optimistic model
        best_arm = 0;
        best_value = 0.0;
        best_theta = 0;
        for m=Theta_t
            %[optmu_m optarm_m] = max(mu(m,:));
            if (optmu(m,1) > best_value)
                best_arm = optmu(m,2);
                best_theta = m;
                best_value = optmu(m,1);
            end
        end
        
    elseif (alg == 3) 
    %% MUCB++
        % compute the active set
        Theta_t = compute_active_set(mu, M, K, hmu, conf);
        
        if (length(Theta_t) > 1)
            
            % compute the arm with the smallest worst-case regret
            arm_regret = zeros(K, 1);
            for i = 1:K
                % compute the additional number of pulls to discard env mm if true environment is
                % m by pulling arm i, and the corresponding worst-case regret
                %tau = zeros(length(Theta_t), length(Theta_t));
%                 for m=1:length(Theta_t)
%                     for mm=1:length(Theta_t)
%                         tau(m, mm) = c2*invSqrGamma(Theta_t(m),Theta_t(mm),i) - pulls(i);
%                         %tau(m, mm) = 4*log(n)/(Gamma(Theta_t(m),Theta_t(mm),i)^2) - pulls(i);
%                         tau(m, mm) = max(min(tau(m,mm), n-t), 0);
%                     end
%                 end
                tau = c2*invSqrGamma(Theta_t,Theta_t,i) - pulls(i);
                tau = max(min(tau, n-t), 0);
                arm_regret(i) = max(Delta(Theta_t,i) .* min(tau, [], 2));
            end
            [best_value best_arm] = min(arm_regret);
        else
            [best_value best_arm] = max(mu(Theta_t, :));
        end

    elseif (alg == 4) 
    %% MUCB++(rho)
        % compute the active set
        Theta_t = compute_active_set(mu, M, K, hmu, conf);

        if (length(Theta_t) > 1)
            % renormalize the distribution (aka, compute posterior)
            trho = zeros(length(Theta_t), 1);
            prob_mass = 0.0;
            for m=1:length(Theta_t)
                prob_mass = prob_mass + rho(Theta_t(m));
                trho(m) = rho(Theta_t(m));
            end
            trho = trho./prob_mass;
            
            % compute the arm with the smallest expected regret          
            arm_regret = zeros(K, 1);
            for i = 1:K
                % compute the additional number of pulls to discard env mm if true environment is
                % m by pulling arm i, and the corresponding worst-case regret
                %tau = zeros(length(Theta_t), length(Theta_t));
%                 for m=1:length(Theta_t)
%                     for mm=1:length(Theta_t)
%                         tau(m, mm) = c2*invSqrGamma(Theta_t(m),Theta_t(mm),i) - pulls(i);
%                         %tau(m, mm) = 4*log(n)/(Gamma(Theta_t(m),Theta_t(mm),i)^2) - pulls(i);
%                         tau(m, mm) = max(min(tau(m,mm), n-t), 0);
%                     end
%                 end
                tau = c2*invSqrGamma(Theta_t,Theta_t,i) - pulls(i);
                tau = max(min(tau, n-t), 0);
                arm_regret(i) = sum(trho .* (Delta(Theta_t,i) .* min(tau, [], 2)));
            end
            [best_value best_arm] = min(arm_regret);
        else
            [best_value best_arm] = max(mu(Theta_t, :));
        end
        
    end
    
    %% generate a sample from the true model and the chosen arm
    sample = (rand < mu(theta, best_arm));
    cumulX(best_arm) = cumulX(best_arm) + sample;
    pulls(best_arm) = pulls(best_arm)+1;
    hmu(best_arm) = cumulX(best_arm)/pulls(best_arm);
    
end

