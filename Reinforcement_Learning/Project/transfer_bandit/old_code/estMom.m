function [M1,M2,M3]=estMom(data,C,N)

M1=zeros(C.K,1);
M2=zeros(C.K,C.K);
M3=zeros(C.K,C.K,C.K);
for i=1:N
M1=M1+data(i).tri(:,1);
M2 = M2+data(i).tri(:,1)*data(i).tri(:,2)';
M2Rep=repmat(data(i).tri(:,1)*data(i).tri(:,2)',[1,1,C.K]); 
M3Rep=repmat(reshape(data(i).tri(:,3),[1,1,C.K]),[C.K,C.K,1]);
M3= M3+M2Rep.*M3Rep;
end;
M1=M1/N;
M2=(M2*M2')^0.5/N;
M3=M3/N;

return;