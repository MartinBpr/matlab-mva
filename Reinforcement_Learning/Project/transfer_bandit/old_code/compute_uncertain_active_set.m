function Theta_t = compute_uncertain_active_set(M, K, hmodel_mu, model_conf, hmu, arm_conf)

Theta_t = [];
for m=1:M
    if (~any(hmodel_mu(m,:)' < hmu-arm_conf-model_conf) ...
        && ~any(hmodel_mu(m,:)' > hmu+arm_conf+model_conf))
        Theta_t = [Theta_t m];
    end

%    is_compatible = true;
%     i = 1;
%     while is_compatible && i<=K
%         if (mu(m,i) > hmu(i)+conf(i) || mu(m,i) < hmu(i) - conf(i))
%             i = i+1;
%         end
%     end
%     if (is_compatible)
%         Theta_t = [Theta_t m];
%     end
end

%if (isempty(Theta_t))
%    Theta_t = theta;
%end
