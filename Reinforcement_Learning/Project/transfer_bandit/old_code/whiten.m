function [WM,W,Dw]=whiten(M2,M3,C)

[W1,Dw]=eigs(M2,C.m);

%W1=V(:,1:C.m);
%Dw=D(1);
W=W1*(Dw^(-0.5));
%W'*M2*W

WM1=zeros(C.K,C.K,C.m);
WM2=zeros(C.K,C.m,C.m);
WM=zeros(C.m,C.m,C.m);

for i=1:C.m
    for j=1:C.K
    WM1(:,:,i)=WM1(:,:,i)+M3(:,:,j)*W(j,i);
    end;  
end;

for i=1:C.m
    for j=1:C.K
    WM2(:,i,:)=WM2(:,i,:)+WM1(:,j,:)*W(j,i);
    end;
end;


for i=1:C.m
    for j=1:C.K
    WM(i,:,:)=WM(i,:,:)+WM2(j,:,:)*W(j,i);
    end;
end;




return;

