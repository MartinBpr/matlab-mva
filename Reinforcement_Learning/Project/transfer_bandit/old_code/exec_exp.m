function avg_pulls = exec_exp(mu, optmu, Gamma, invSqrGamma, Delta, rho, theta, K, M, runs, n, alg)

avg_pulls = zeros(K, 1);
parfor r = 1:runs
    %fprintf('%d', r);
    avg_pulls = avg_pulls + exec_run(mu, optmu, Gamma, invSqrGamma, Delta, rho, theta, K, M, n, alg);
    %fprintf([repmat(8,1,numel(num2str(r))) ''])
end

avg_pulls = avg_pulls./runs;