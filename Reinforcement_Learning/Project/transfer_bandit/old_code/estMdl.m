function [M1,M2,M3]=estMdl(data,N)

M1=zeros(C.d,1);
M2=zeros(C.d,C.d);
M3=zeros(C.d,C.d,C.d);
for i=1:N
M1=mean([data.tri(1,:)],2);
M2 = M2+data(i).tri(:,1)*data(i).tri(:,2);
M2Rep=repmat(M2,[1,1,C.d]); 
M3Rep=repmat(reshape(data(i).tri(:,3),[1,1,C.d]),[C.d,C.d,1]);
M3= M3+M2Rep.*M3Rep;
end;
M2=M2/N;
M3=M3/N;