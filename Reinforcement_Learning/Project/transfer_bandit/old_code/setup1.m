format compact;

% s = RandStream('mcg16807','Seed',0)
% RandStream.setDefaultStream(s)

K = 5;
M = 3;

mu = zeros(M, K);
mu(1, 1) = 0.9;
mu(1, 2) = 0.8;
mu(1, 3) = 0.7;
mu(1, 4) = 0.1;
mu(1, 5) = 0.2;

mu(2, 1) = 0.89;
mu(2, 2) = 0.79;
mu(2, 3) = 0.71;
mu(2, 4) = 0.2;
mu(2, 5) = 0.1;

mu(3, 1) = 0.85;
mu(3, 2) = 0.88;
mu(3, 3) = 0.1;
mu(3, 4) = 0.05;
mu(3, 5) = 0.01;

% model gaps
Gamma = zeros(M,M,K);
invSqrGamma = zeros(M,M,K);
for i=1:K
    for m=1:M
        for mm=1:M
            Gamma(m,mm,i) = abs(mu(m,i) - mu(mm,i));
            invSqrGamma(m,mm,i) = 1/(Gamma(m,mm,i)^2);
        end
    end
end

% arm gaps
Delta = zeros(M,K);
for i=1:K
    for m=1:M
        Delta(m,i) = max(mu(m,:) - mu(m,i));
    end
end

% compute the optimal arm and the optimal value for each model
optmu = zeros(M, 2);
for m=1:M
    % value and arm
    [optmu(m,1) optmu(m,2)] = max(mu(m,:));
end

% distribution over models
rho = [0.15 0.15 0.7];

% horizon
n_values = [10000:5000:50000];

% number of runs
runs = 50;

% regret for each of the algorithms
bayes_regret = zeros(length(n_values), 4);
model_regret = zeros(M, length(n_values), 4);

% list of the algoritms to run
algs = [1 2 3 4];

% for each possible "true" model run all the algorithms and compute the
% regret
for alg = algs
    for theta=1:M
        disp(['Running algorithm ' num2str(alg) ' on model ' num2str(theta)]);
        
        for s=1:length(n_values)
            disp(['n=' num2str(n_values(s))]);
            avg_pulls = exec_exp(mu, optmu, Gamma, invSqrGamma, Delta, rho, theta, K, M, runs, n_values(s), alg)';
            for i=1:K
                model_regret(theta,s,alg) = model_regret(theta,s,alg) + avg_pulls(i)*(optmu(theta,1) - mu(theta, i));
            end
            disp(['==> R=' num2str(model_regret(theta,s,alg))]);
            bayes_regret(s,alg) =  bayes_regret(s, alg) + rho(theta)*model_regret(theta,s,alg);
        end
    end
    disp(['==> BR=' num2str(bayes_regret(:,alg)')]);
end
