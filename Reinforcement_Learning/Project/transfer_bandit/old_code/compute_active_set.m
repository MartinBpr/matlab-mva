function Theta_t = compute_active_set(mu, M, K, hmu, conf)

Theta_t = [];
for m=1:M
    if (~any(mu(m,:)' < hmu-conf) && ~any(mu(m,:)' > hmu+conf))
        Theta_t = [Theta_t m];
    end

%    is_compatible = true;
%     i = 1;
%     while is_compatible && i<=K
%         if (mu(m,i) > hmu(i)+conf(i) || mu(m,i) < hmu(i) - conf(i))
%             i = i+1;
%         end
%     end
%     if (is_compatible)
%         Theta_t = [Theta_t m];
%     end
end

if (isempty(Theta_t))
    Theta_t = theta;
end
