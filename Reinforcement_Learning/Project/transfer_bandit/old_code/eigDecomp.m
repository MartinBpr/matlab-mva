function [v,lambda]=eigDecomp(M3,C)

v=zeros(C.m,C.m);
lambda=zeros(C.m,1);

for p=1:C.m

theta=zeros(C.m,C.L);
for i=1:C.L
    
    theta0=rand(C.m,1);
    theta(:,i)=theta0/norm(theta0);
    
    
   
    for k=1:C.L1
        
    thM=zeros(C.m,C.m);
    for j=1:C.m
        
          
         thM=thM+reshape(M3(:,:,j)*theta(j,i),C.m,C.m) ;
    end;
    
    
    %thTemp=zeros(C.m,1);
     %for j=1:C.m
        
          
         thTemp=thM*theta(:,i);
    %end;
    
    
    theta(:,i)=thTemp./norm(thTemp);
    end;
    
    
    
    
end;


wT=zeros(C.L,1);

for i=1:C.L
    
    thM=zeros(C.m,C.m);
    for j=1:C.m
        
          
         thM=thM+reshape(M3(:,:,j)*theta(j,i),[C.m,C.m]) ;
    end;
    
    
    %thTemp=zeros(C.m,1);
    %for j=1:C.m
        
          
         thTemp=thM*theta(:,i);
    %end;
    
    wT(i)=thTemp'*theta(:,i);
    
    
    
end;

[lambda(p),ind]=max(wT);


htheta=theta(:,ind);
v(:,p)=htheta;

htheta2=htheta*htheta';
htheta2rep=repmat(htheta2,[1,1,C.m]);
htheta1rep=repmat(reshape(htheta,[1,1,C.m]),[C.m,C.m,1]);
htheta3=htheta2rep.*htheta1rep;
M3=M3-lambda(p)*htheta3;



end;

return;
