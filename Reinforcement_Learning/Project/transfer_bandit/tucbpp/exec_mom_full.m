function [ hrho, model_hmu, rho_conf, model_conf ] = exec_mom(setting, samples)

%Inputs:

%%%%% dataC is a N*K cell array where N is   the number of tasks
%%%%% (so far ) and K is the number of arms
%%%each entry of the cell is a vector consisits of all the samples for each arm and
%%%each task. 

%%% K  number of arms

%%% m number of models

%%% rho the true distributions on the models

%%% mumat the true average rewards for all the models and all the arms (We
%%% need these two (mu, rho) to artificially make the confidence intervals since
%%% we do not yet have thecomplete theoretical bounds )



%Outputs:

%%%%rhohat: Emprirical rho

%%%% muhat: Empirical mu for all the models and arms it is a true mpirical
%%%% estimation up to a permutation in m

%%% errMu maximum error per arm per model for the estimated average reward

%%% errRho maximum error per model for the esimated rho



C.K=setting.K;
C.m=setting.M;
C.L=200;
C.L1=200;
C.mu=setting.mu;
C.rho=setting.rho;
%sampNum=8;
%NVect=logspace(1,sampNum/2,sampNum);
model_conf=0;
rho_conf=0;
N=size(samples,1);


for i=1:N
    data(i).tri=zeros(C.K,3);
    for j=1:C.K
        data(i).tri(j,:)=samples{i,j}(1:3);
    end
end;

[M1,M2,M3]=estMom(data,C,N);


[WM,W,D]=whiten(M2,M3,C);
[v,lambda]=eigDecomp(WM,C);


model_hmu=(ones(C.K,1)*lambda'.*(pinv(W')*v))';
hrho=1./(lambda.^2);

for j=1:C.m
    
    distMin=norm(model_hmu+C.mu);
    distMinRho=1;
    
    for k=1:C.m
        distMin=min(max(abs((model_hmu(:,j)-C.mu(:,k)))),distMin);
        distMinRho=min(abs(hrho(j)-C.rho(k)),distMinRho);
    end;
    model_conf=max(model_conf,distMin);
    rho_conf=max(rho_conf,distMinRho);
    
end;
