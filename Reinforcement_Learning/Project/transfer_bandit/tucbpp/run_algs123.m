format compact;

algs = [7];
%n_values = [5000 10000 15000 20000]
n_values = [2500,5000,7500,10000];
settings = 2;
J = 2500;
runs = 50;
%regretSum=zeros(J,1);


for s=settings
    for alg=algs
        for n=n_values
            for r=1:runs
                regret=run_experiment(s, n, J, alg, 0); 
                regret.nv(n).rv(r) = regret;
            end
           
        end
    end
end

%plot( smooth( regretSum/runs , 5 ) )





