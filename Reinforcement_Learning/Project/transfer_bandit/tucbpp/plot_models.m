function plot_models(figure1, M, K, mu, optarm)

clf;

% Create axes
n_ticks = 1;
arm_ticks_pos = 1:K;
model_ticks_pos = zeros(M,K);
all_ticks_pos = 0;
for i=1:K
    for m=1:M
        %ticks_labels{n_ticks} = ['A' num2str(i) '-m' num2str(m)];
        ticks_labels{n_ticks} = ['m' num2str(m)];
        model_ticks_pos(m,i) = i+(m-1)/(M+2);
        all_ticks_pos(n_ticks) = i+(m-1)/(M+2);
        n_ticks = n_ticks+1;
    end
end

% list of colors
color_list=prism(M);
color_list(3,:) = [0 1 1];

axes1 = axes('Parent',figure1,'XTickLabel',ticks_labels,'XTick',all_ticks_pos,'FontSize',12);
box(axes1,'on');
hold(axes1,'all');
min_y = 0;
max_y = 1.0;
ylim(axes1,[min_y max_y]);
xlim(axes1,[0.5 K+1.0]);

% Create xlabel
xlabel('Models','FontSize',16);

% Create ylabel
ylabel('Reward','FontSize',16);

% Create confidence intervals and true means for models
line([model_ticks_pos(1,1)-0.2 model_ticks_pos(1,1)-0.2],get(axes1,'YLim'),'LineWidth',2,'Color',[0 0 0])
for i=1:K
    for m=1:M
        if(i == optarm(m))
            plot(model_ticks_pos(m,i),mu(m,i),'MarkerSize',22,'Marker','s','LineWidth',5,'LineStyle','none',...
                'Color',color_list(m,:));
        else
            plot(model_ticks_pos(m,i),mu(m,i),'MarkerSize',16,'Marker','o','LineWidth',3,'LineStyle','none',...
                'Color',color_list(m,:));
%            plot(model_ticks_pos(m,i),mu(m,i),'MarkerSize',7,'Marker','o','LineWidth',2,'LineStyle','none');

        end
    end
    line([model_ticks_pos(M,i)+0.2 model_ticks_pos(M,i)+0.2],get(axes1,'YLim'),'LineWidth',2,'Color',[0 0 0])
end

drawnow;

