% horizon
n_values = [1000 2000 5000 10000 15000];

% setting
setting_idx = 2;

% list of the algoritms to run
algs = [1 2 3];
n_algs = length(algs);

% number of episodes
J = 20;

% directory
dir_name = ['/Users/leto/transfer_results_alg123/'];

avg_regret = zeros(n_algs, length(n_values), J);
stddev_regret = zeros(n_algs, length(n_values), J);
avg_exp_comp = zeros(n_algs, length(n_values), J);
model_errors = zeros(n_algs, length(n_values), J);
tot_runs = zeros(n_algs, length(n_values));
for a = 1:n_algs
    for n = 1:length(n_values)
        [avg_regret(a,n,:) stddev_regret(a,n,:) avg_exp_comp(a,n,:) model_errors(a,n,:) tot_runs(a, n)] = ...
            read_results(algs(a), n_values(n), J, setting_idx, dir_name);
    end
end

avg_regret_ucb = mean(reshape(avg_regret(1,:,:), length(n_values), J), 2);
avg_regret_ucb_opt = mean(reshape(avg_regret(2,:,:), length(n_values), J), 2);
avg_regret_mucb = mean(reshape(avg_regret(3,:,:), length(n_values), J), 2);

n_idx = length(n_values);
ucb_complexity = reshape(avg_exp_comp(1, n_idx, :), 1, J);
ucb_opt_complexity = reshape(avg_exp_comp(2, n_idx, :), 1, J);
mucb_complexity = reshape(avg_exp_comp(3, n_idx, :), 1, J);

% list of the algoritms to run
algs = [4];
n_algs = length(algs);

% number of episodes
J_values = [1000 2000 5000];

% directory
dir_name = ['/Users/leto/transfer_results_alg123/'];

cumul_regret = zeros(3+length(J_values), length(n_values));
cumul_regret(1,:) = avg_regret_ucb;
cumul_regret(2,:) = avg_regret_ucb_opt;
cumul_regret(3,:) = avg_regret_mucb;

for J_idx=1:length(J_values)
    J = J_values(J_idx);
    avg_regret = zeros(n_algs, length(n_values), J);
    stddev_regret = zeros(n_algs, length(n_values), J);
    avg_exp_comp = zeros(n_algs, length(n_values), J);
    model_errors = zeros(n_algs, length(n_values), J);
    tot_runs = zeros(n_algs, length(n_values));
    for a = 1:n_algs
        for n = 1:length(n_values)
            [avg_regret(a,n,:) stddev_regret(a,n,:) avg_exp_comp(a,n,:) model_errors(a,n,:) tot_runs(a, n)] = ...
                read_results(algs(a), n_values(n), J, setting_idx, dir_name);
        end
    end
    
    for n_idx=1:length(n_values)
        if (J_values(J_idx) == 5000 && n_values(n_idx) == 15000)
            all_regrets = reshape(avg_regret(:,n_idx,:), n_algs, J);

            sub_sample = ceil(J_values(J_idx)/12);

            all_complexities = reshape(avg_exp_comp(:, n_idx, :), n_algs, J);
            complexity_comparison = [ucb_complexity(1)*ones(1,J); ucb_opt_complexity(1)*ones(1,J); mucb_complexity(1)*ones(1,J); all_complexities];
            
            plot_complexity_comparison([1:sub_sample:J J], blockmean(complexity_comparison, sub_sample), ['n=' num2str(n_values(n_idx)) '; J=' num2str(J)]);

            regret_comparison = [avg_regret_ucb(n_idx,:)*ones(1,J); avg_regret_ucb_opt(n_idx,:)*ones(1,J); avg_regret_mucb(n_idx,:)*ones(1,J); all_regrets];
            plot_episode_regret([1:sub_sample:J J], blockmean(regret_comparison, sub_sample), ['n=' num2str(n_values(n_idx)) '; J=' num2str(J)]);
        end
        
        cumul_regret(3+J_idx,n_idx) = mean(avg_regret(1,n_idx,:));
    end
end

plot_comparison_n_all(n_values, cumul_regret');
