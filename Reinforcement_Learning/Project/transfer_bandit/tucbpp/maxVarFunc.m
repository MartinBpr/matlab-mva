function maxVar=maxVarFunc(meanMat, rho)
m=size(meanMat,1);
K=size(meanMat,2);
EX22=zeros(K);
EX2=zeros(K);

for i=1:m
    for j=1:K
        for t=1:K
         EX2(j,t)=EX2(j,t)+(meanMat(i,j) * meanMat( i,t ))/m;
         EX22(j,t)=EX22(j,t)+(meanMat(i,j) * meanMat( i,t ))^2/m;
        end;
    end;
    
end;
max2Var=max(max(EX22-EX2.^2));


EX32=zeros(K,K,K);
EX3=zeros(K,K,K);

for i=1:m
    for j=1:K
        for t=1:K
            for p=1:K
         EX3(j,t,p)=EX3(j,t,p)+(meanMat(i,j) * meanMat( i,t )* meanMat( i,p ))*rho(i);
         EX32(j,t,p)=EX32(j,t,p)+(meanMat(i,j) * meanMat( i,t )* meanMat( i,p ))^2*rho(i);
            end;
        end;
    end;
    
end;

max3Var=max(max( max(EX32-EX3.^2) ));
maxVar=max(max3Var,max2Var);

