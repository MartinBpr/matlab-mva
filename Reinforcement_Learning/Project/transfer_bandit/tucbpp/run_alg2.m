format compact;
clear all
rng(2)

algs = [2];
%n_values = [5000 10000 15000 20000]
n_values = [2500,5000,7500,10000];
settings = 2;
J = 5000;
runs = 20;
%J = 0;
%runs = 2;
%n_values=100;
%regretSum=zeros(J,1);

for s=settings
    for alg=algs
        nInd=0;

        for n=n_values
            nInd=nInd+1;
            rInd=0;
            for r=1:runs
                rInd=rInd+1;
                regret=run_experiment(s, n, J, alg, 0); 
                regretSt.nv(nInd).rv(rInd).reg = regret;
            end
           
        end
    end
end

save(['ResAug23',num2str(algs)],  'regretSt' )

%plot( smooth( regretSum/runs , 5 ) )





