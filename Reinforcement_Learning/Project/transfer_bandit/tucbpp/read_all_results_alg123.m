% horizon
n_values = [1000 2000 5000 10000 20000 30000 40000 50000 60000 70000 80000 90000 100000];

% setting
setting_idx = 2;

% list of the algoritms to run
algs = [1 2 3];
n_algs = length(algs);

% number of episodes
J = 20;

% directory
dir_name = ['/Users/leto/transfer_results_alg123/'];

avg_regret = zeros(n_algs, length(n_values), J);
stddev_regret = zeros(n_algs, length(n_values), J);
avg_exp_comp = zeros(n_algs, length(n_values), J);
model_errors = zeros(n_algs, length(n_values), J);
tot_runs = zeros(n_algs, length(n_values));
for a = 1:n_algs
    for n = 1:length(n_values)
        [avg_regret(a, n, :) stddev_regret(a, n, :) avg_exp_comp(a, n, :) model_errors(a,n,:) tot_runs(a, n)] = ... 
            read_results(algs(a), n_values(n), J, setting_idx, dir_name);
    end
end

avg_regret_ucb = mean(reshape(avg_regret(1,:,:), length(n_values), J), 2);
avg_regret_ucb_opt = mean(reshape(avg_regret(2,:,:), length(n_values), J), 2);
avg_regret_mucb = mean(reshape(avg_regret(3,:,:), length(n_values), J), 2);
plot_comparison_n(n_values, [avg_regret_ucb avg_regret_ucb_opt avg_regret_mucb]);
