format compact;

seed = RandStream('mcg16807','Seed',0);
RandStream.setDefaultStream(seed);

s = gen_setting1();

J = 10;
pulls = [3:1:15]';
pulls = 3;
n_tests = length(pulls);
model_errors = zeros(n_tests, J);

runs = 10;
btheta = discreternd(s.rho, J);

for r=1:runs
    disp(['Run ' num2str(r)]);
    errors = zeros(n_tests, J);
    for k=1:n_tests
        M2 = zeros(s.K,s.K);
        M3 = zeros(s.K,s.K,s.K);
        for j=1:J
            samples = {};
            for i=1:s.K
                samples{i} = (rand(pulls(k),1) < s.mu(btheta(j), i));
            end
            [hrho, model_hmu, rho_conf, bound_errors, errors(k,j), M2, M3] = exec_mom(s, samples, M2, M3, j);
        end
        disp(['Test ' num2str(k)]);
    end
    model_errors = model_errors + errors;
end

model_errors = model_errors./runs;