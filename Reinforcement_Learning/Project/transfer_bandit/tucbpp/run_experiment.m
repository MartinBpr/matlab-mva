function regret=run_experiment(setting_idx, n, J, alg, running_octave)

format compact;

if(running_octave == 0)
    %seed = RandStream('mcg16807','Seed',0)
    %RandStream.setDefaultStream(seed)
    mtstream = RandStream('mt19937ar');
    RandStream.setDefaultStream(mtstream);
    rng shuffle
end

if (setting_idx == 1)
    s = gen_setting1();
elseif (setting_idx == 2)
    s = gen_setting2();
end

experiment.setting = s;

% length of each episode
experiment.n = n;

% number of episodes
experiment.J = J;

% list of the algoritms to run
experiment.alg = alg;

% sequence of tasks
btheta = discreternd(experiment.setting.rho, experiment.J);

disp(['Running algorithm ' num2str(experiment.alg) ' on experiment']);
disp(experiment);
disp(experiment.setting);
[pulls btheta model_hmu model_conf comp exp_comp model_errors] = exec_exp(experiment, btheta);

% compute the regret from the number of pulls and the sequence of tasks
regret = zeros(experiment.J,1);
for j=1:experiment.J
    for i=1:s.K
        regret(j) = regret(j) + pulls(j,i)*(s.optmu(btheta(j)) - s.mu(btheta(j), i));
    end
end

% save structures
file_exists = 1;
idx = 1;

%    file_name = ['results_set' num2str(setting_idx) '_alg' num2str(alg) '_n' num2str(n) '_J' num2str(J) '_' num2str(idx) '.mat'];
%    a=dir;
%    b=struct2cell(a);
%    if (any(ismember(b(1,:),file_name)))
%        file_exists = 1;
%        idx = idx+1;
%    else
%        file_exists = 0;
%    end
%end
%save ('-mat-binary', file_name, '-struct', 'regret');
%if (running_octave == 0)
%    save (file_name, 'regret', 'exp_comp', 'model_errors');
%else
%    save ('-mat-binary', file_name, 'regret', 'exp_comp', 'model_errors');
%end
