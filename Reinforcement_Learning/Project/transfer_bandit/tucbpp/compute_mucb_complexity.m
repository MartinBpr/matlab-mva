function [complexity Gamma_min Theta_opt] = compute_mucb_complexity(setting, btheta)

Gamma_min = 1.0;
complexity = 0.0;

% set of optimistic models
Theta_opt = find(setting.optmu >= setting.optmu(btheta))';
for i=1:setting.K
    if (i ~= setting.optarm(btheta))
        % search for all the optimistic models with i as optimal arm
        Theta_i = find(setting.optarm(Theta_opt) == i)';
        if ~isempty(Theta_i)
            Gamma_i_min = 1.0;
            for theta_idx = Theta_i
                theta = Theta_opt(theta_idx);
                if (theta ~= btheta)
                    g = setting.Gamma(theta, btheta, i);
                    if (g < Gamma_i_min)
                        Gamma_i_min = g;
                    end
                    if (g < Gamma_min)
                        Gamma_min = g;
                    end
                end
            end
            complexity = complexity + setting.Delta(btheta,i)/(Gamma_i_min^2);
        end
    end
end