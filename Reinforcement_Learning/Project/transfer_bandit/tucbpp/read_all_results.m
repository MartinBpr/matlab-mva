% horizon
n_values = [500 1000 2000 5000 10000 20000];
%n_values = [500 1000 2000 5000];
%n_values = [500];

% setting
setting_idx = 2;

% list of the algoritms to run
algs = [1 2 3];
n_algs = length(algs);

% number of episodes
J = 10;

avg_regret = zeros(n_algs, length(n_values), J);
stddev_regret = zeros(n_algs, length(n_values), J);
avg_exp_comp = zeros(n_algs, length(n_values), J);
tot_runs = zeros(n_algs, length(n_values));
for a = 1:n_algs
    for n = 1:length(n_values)
        [avg_regret(a, n, :) stddev_regret(a, n, :) avg_exp_comp(a, n, :) tot_runs(a, n)] = ... 
            read_results(algs(a), n_values(n), J, setting_idx);
    end
end

avg_regret_ucb = mean(reshape(avg_regret(1,:,:), length(n_values), J), 2);
avg_regret_ucb_opt = mean(reshape(avg_regret(2,:,:), length(n_values), J), 2);
avg_regret_mucb = mean(reshape(avg_regret(3,:,:), length(n_values), J), 2);
plot_comparison_n(n_values, [avg_regret_ucb avg_regret_ucb_opt avg_regret_mucb]);

n_idx = length(n_values);

figure;
all_regrets = reshape(avg_regret(:,n_idx,:), n_algs, J);
plot(1:J, all_regrets);

figure
all_complexities = reshape(avg_exp_comp(:, n_idx, :), n_algs, J);
plot(1:J, all_complexities);

figure
regret_comparison = [avg_regret_ucb(n_idx,:)*ones(1,J); avg_regret_mucb(n_idx,:)*ones(1,J); all_regrets(4,:); all_regrets(5,:)];
sub_sample = [1:50:J];
plot_episode_regret(sub_sample, regret_comparison(:, sub_sample));
