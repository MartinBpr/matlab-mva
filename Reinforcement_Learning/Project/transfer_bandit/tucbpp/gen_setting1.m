function s = gen_setting1()

% number of arms
s.K = 7;

% number of models
s.M = 5;

s.mu = zeros(s.M, s.K);

% model 1
s.mu(1, 1) = 0.9;
s.mu(1, 2) = 0.85;
s.mu(1, 3) = 0.6;
s.mu(1, 4) = 0.35;
s.mu(1, 5) = 0.35;
s.mu(1, 6) = 0.35;
s.mu(1, 7) = 0.35;

% model 2
s.mu(2, 1) = 0.8;
s.mu(2, 2) = 0.89;
s.mu(2, 3) = 0.6;
s.mu(2, 4) = 0.35;
s.mu(2, 5) = 0.35;
s.mu(2, 6) = 0.35;
s.mu(2, 7) = 0.35;

% model 3
s.mu(3, 1) = 0.1;
s.mu(3, 2) = 0.3;
s.mu(3, 3) = 0.6;
s.mu(3, 4) = 0.3;
s.mu(3, 5) = 0.2;
s.mu(3, 6) = 0.1;
s.mu(3, 7) = 0.05;

% model 4
s.mu(4, 1) = 0.5;
s.mu(4, 2) = 0.5;
s.mu(4, 3) = 0.6;
s.mu(4, 4) = 0.625;
s.mu(4, 5) = 0.45;
s.mu(4, 6) = 0.45;
s.mu(4, 7) = 0.4;

% model 5
s.mu(5, 1) = 0.1;
s.mu(5, 2) = 0.3;
s.mu(5, 3) = 0.6;
s.mu(5, 4) = 0.1;
s.mu(5, 5) = 0.9;
s.mu(5, 6) = 0.6;
s.mu(5, 7) = 0.3;

% distribution over models
%s.rho = [0.15 0.1 0.45 0.15 0.15];
s.rho = (1/s.M)*ones(1,s.M);

% compute the optimal arm and the optimal value for each model
s.optmu = zeros(s.M, 1);
s.optarm = zeros(s.M, 1);
for m=1:s.M
    % value and arm
    [s.optmu(m) s.optarm(m)] = max(s.mu(m,:));
end

% model gaps
s.Gamma = zeros(s.M,s.M,s.K);
for i=1:s.K
    for m=1:s.M
        for mm=1:s.M
            s.Gamma(m,mm,i) = abs(s.mu(m,i) - s.mu(mm,i));
        end
    end
end

% arm gaps
s.Delta = zeros(s.M,s.K);
for i=1:s.K
    for m=1:s.M
        s.Delta(m,i) = s.optmu(m,:) - s.mu(m,i);
    end
end


