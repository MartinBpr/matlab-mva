
fig = figure('XVisual','');
arm_hmu = 0.5.*ones(s.K,1);
arm_conf = 0.5.*ones(s.K,1);
tmp_pulls = ones(s.K,1);
Theta_t = compute_uncertain_active_set(s.M, model_hmu, model_conf, arm_hmu, arm_conf);
plot_arms(fig, s.M, s.K, s.mu, btheta, arm_hmu, tmp_pulls, arm_conf, model_hmu, model_conf, Theta_t);