algs=(4)
n_values=(1000 2000 5000 10000 15000)
J_values=(1000 2000 5000)

for k in "${J_values[@]}"
do
	for j in "${algs[@]}"
	do
		for i in "${n_values[@]}"
		do
			echo "Generate experiment alg="$j" n="$i" J="$k 
			launch_file="launch_set2_alg"$j"_n"$i"_J"$k".m"
			echo $launch_file
			sed "2s/.*/n = $i;/"   launch.m > tmp.m
			sed "3s/.*/J = $k;/"   tmp.m > tmp1.m
			sed "4s/.*/alg = $j;/" tmp1.m > tmp2.m
            sed "5s/.*/running_octave = 1;/" tmp2.m > $launch_file
			rm tmp.m tmp1.m tmp2.m
		
			grid_launch_file="launch_transfer_set2_alg"$j"_n"$i"_J"$k".sh"
			echo "sleep \$(( \$RANDOM % 100 ))" > $grid_launch_file
			echo "export PATH=\$PATH:/home/alazaric/octave/bin:/home/alazaric/transfer_bandit/" >> $grid_launch_file
			echo "echo \$PATH" >> $grid_launch_file
			echo "cd /home/alazaric/transfer_bandit/" >> $grid_launch_file
			echo "octave -q "$launch_file" > output.out\$OAR_ARRAY_INDEX" >> $grid_launch_file
			echo "cd" >> $grid_launch_file
			chmod a+x $grid_launch_file
		done
	done
done
