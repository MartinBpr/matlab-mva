function Theta_t = compute_active_set(M, mu, hmu, conf)

Theta_t = [];
for m=1:M
    if all( (abs(mu(m,:)'-hmu)-conf < 0.0) )
        Theta_t = [Theta_t m];
    end
end

if (isempty(Theta_t))
    % return the closest model to the current estimates
    theta_min = 1;
    theta_val = 1.0;
    for m=1:M
        v = sum(abs(mu(m,:)'-hmu));
        if (v < theta_val)
            theta_min = m;
            theta_val = v;
        end
    end
    Theta_t = theta_min;
end
