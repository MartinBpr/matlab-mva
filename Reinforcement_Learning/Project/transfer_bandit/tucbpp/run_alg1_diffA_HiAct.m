format compact;
clear all
rng(2)

algs = [1];
%n_values = [5000 10000 15000 20000]
n_values = [5000];
k_values =[28, 35];
settings = 2;
J = 5000;
runs = 20;

%J = 10;
%runs = 2;
%n_values=100;
%regretSum=zeros(J,1);
Nc=25;


for s=settings
    for alg=algs
        nInd=0;
        for n=n_values
            nInd=nInd+1;
            kInd=0;
            for  K=k_values
            kInd=kInd+1;
            rInd=0;
            for r=1:runs
                rInd=rInd+1;
                regret=run_experiment_diffAct(s, n, J, K, Nc, alg, 0); 
                regretSt.nv(nInd).kv(kInd).rv(rInd).reg = regret;
            end
            end;
        end
    end
end



save(['ResAug25_diffA_HiAct',num2str(algs)],  'regretSt' )

%plot( smooth( regretSum/runs , 5 ) )





