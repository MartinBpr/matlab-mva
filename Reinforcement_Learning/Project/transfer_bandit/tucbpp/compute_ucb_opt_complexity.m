function [complexity Delta_min] = compute_ucb_opt_complexity(setting, btheta)

optimal_arms = [];
for i=1:setting.K
    if (~isempty(find(setting.optarm == i)))
        optimal_arms = [optimal_arms i];
    end
end

optimal_arms(find(optimal_arms == setting.optarm(btheta))) = [];
deltas = setting.Delta(btheta,optimal_arms);
Delta_min = min(deltas);
complexity = sum(1./deltas);
