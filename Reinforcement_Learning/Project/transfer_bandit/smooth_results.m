function smoothed_results = smooth_results(results, w, c)

J = length(results);

smoothed_results = zeros(1, J-w);

for j = 1:(J-w)
    window = results(j:(j+w));
    for i = 1:(w-c)
       [~, a] = max(window - mean(window));
       window = window([1:(a-1), (a+1):length(window)]);

    end
    smoothed_results(j) = mean(window);
end

%plot(1:(J-w), smoothed_results)