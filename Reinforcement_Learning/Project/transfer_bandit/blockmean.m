function v = blockmean(x, block_size)

n=size(x, 2);
n_blocks = floor(n/block_size);
v = zeros(size(x,1), n_blocks+1);
v(:,1) = x(:,1);

for b=1:n_blocks
    interval = (1+(b-1)*block_size):(b*block_size);
    v(:,b+1) = mean(x(:,interval), 2);
end
if(b*n_blocks < n)
    interval = (1+(b-1)*block_size):n;
    v(:,n_blocks+2) = mean(x(:,interval), 2);
end
