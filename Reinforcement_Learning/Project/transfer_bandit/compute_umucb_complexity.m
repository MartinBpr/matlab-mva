function [complexity] = compute_umucb_complexity(setting, model_hmu, model_conf, btheta)
%function [Gamma_min Delta_min A_opt Theta_opt] = compute_umucb_complexity(setting, model_hmu, model_conf, btheta)

s = setting;
% 
% fig = figure('XVisual','');
% arm_hmu = 0.5.*ones(s.K,1);
% arm_conf = 0.5.*ones(s.K,1);
% tmp_pulls = ones(s.K,1);
% Theta_t = compute_uncertain_active_set(s.M, model_hmu, model_conf, arm_hmu, arm_conf);
% plot_arms(fig, s.M, s.K, s.mu, btheta, arm_hmu, tmp_pulls, arm_conf, model_hmu, model_conf, Theta_t);

% for each model it computes the set of non-dominated arms, the set of 
% non-dominated optimistic arms, and the set of dominated arms
optimal_arms = {};
optmistic_arms = {};
dominated_arms = {};

for m=1:s.M
    optimal_arms{m} = [];
    optimistic_arms{m} = [];
    dominated_arms{m} = [];
    % for each arm checks whether it is dominated and if not if it is
    % optimistic
    for i=1:s.K
        is_dominated = 0;
        for j=1:s.K
            if (model_hmu(m,i)+model_conf < model_hmu(m,j)-model_conf)
                is_dominated = 1;
            end            
        end
        if (is_dominated == 1)
            dominated_arms{m} = [dominated_arms{m} i];
        else
            optimal_arms{m} = [optimal_arms{m} i];
            if (model_hmu(m,i) + model_conf > s.optmu(btheta))
                optimistic_arms{m} = [optimistic_arms{m} i];
            end
        end
    end
end

% compute the set of optimistic models and the set of discardable
% optimistic models
Theta_opt = [];
Theta_non_discard_opt = [];
Theta_discard_opt = [];
for m=1:s.M
    if ~isempty(optimistic_arms{m})
        Theta_opt = [Theta_opt m];
        
        is_discardable = 0;
        for i=1:s.K
            if (model_hmu(m,i) + model_conf < s.mu(btheta,i) || model_hmu(m,i) - model_conf > s.mu(btheta,i))
                is_discardable = 1;
            end
        end
        if (is_discardable == 1)
            Theta_discard_opt = [Theta_discard_opt m];
        else
            Theta_non_discard_opt = [Theta_non_discard_opt m];
        end
    end
    
end


% compute the complexity of the problem
complexity = 0.0;
for i=1:s.K
    if (i ~= s.optarm(btheta))
        % there are three categories of arms:
        % - arms that are never pulled
        % - arms that can lead to discard a model
        % - arms that are pulled like in UCB

        delta = s.Delta(btheta, i);
        gamma_min = 1.0;
        is_pulled = 0;
        arm_complexity = 0.0;

        is_pulled_by_discardable = 0;
        for m=Theta_discard_opt
            if ~isempty(find(optimistic_arms{m} == i))
                gamma_min = min(s.Gamma(m, btheta, i), gamma_min);
                is_pulled_by_discardable = 1;
            end
        end
        
        is_pulled_by_non_discardable = 0;
        for m=Theta_non_discard_opt
            if ~isempty(find(optimistic_arms{m} == i))
                is_pulled_by_non_discardable = 1;
            end
        end
        
        if (is_pulled_by_non_discardable == 1)
            arm_complexity = 1/delta;
        elseif (is_pulled_by_non_discardable == 0 && is_pulled_by_discardable == 1)
            if (gamma_min-model_conf < 0.0)
                arm_complexity = 1/delta;
            else
                arm_complexity = delta * min(1/delta^2, 1/(gamma_min-model_conf)^2);
            end
        else
            arm_complexity = 0.0;
        end
        
        complexity = complexity + arm_complexity;
    end
end
