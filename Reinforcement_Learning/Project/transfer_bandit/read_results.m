function [avg_regret stddev_regret avg_exp_comp avg_model_errors tot_runs] = read_results(alg, n, n_episodes, setting_idx, dir_name)

% load structures

regrets = [];
exp_comps = [];
errors = [];

tot_tot_runs = 0;

sub_folders = {''; 'data_1/'; 'data_2/'; 'data_3/'};
for f = 1:length(sub_folders)
    tot_runs = 1;
    sub_folder = sub_folders{f};
    file_exists = 1;
    current_dirname = [dir_name sub_folder];
    a=dir(current_dirname);
    b=struct2cell(a);
    while (file_exists)
        file_name = ['results_set' num2str(setting_idx) '_alg' num2str(alg) '_n' num2str(n) '_J' num2str(n_episodes) '_' num2str(tot_runs) '.mat'];
    %    file_name = ['exp_results' num2str(tot_runs) '.mat'];
        
        if (any(ismember(b(1,:),file_name)))
            disp(['Reading ' current_dirname file_name]);
            load([current_dirname file_name], '-mat', 'regret', 'exp_comp', 'model_errors');
            %full_name = [dir_name file_name];
            %load(full_name);
            regrets = [regrets regret];
            exp_comps = [exp_comps exp_comp];
            errors = [errors model_errors];
            file_exists = 1;
            tot_runs = tot_runs+1;
        else
            file_exists = 0;
        end
    end
    tot_tot_runs = tot_tot_runs + tot_runs - 1;
end
tot_runs = tot_tot_runs;

if (tot_runs == 0)
    disp(['Found nothing about ' file_name]);
end

% regret for each of the algorithms
avg_regret = mean(regrets, 2);
alg, size(regrets), size(avg_regret)
stddev_regret = std(regrets, 1, 2);
avg_exp_comp = mean(exp_comps, 2);
avg_model_errors = mean(errors, 2);
