function Theta_t = compute_uncertain_active_set(M, model_hmu, model_conf, arm_hmu, arm_conf)

Theta_t = [];
for m=1:M
    if all( (abs(model_hmu(m,:)'-arm_hmu)-(arm_conf+model_conf) < 0.0) )
        Theta_t = [Theta_t m];
    end
end

if (isempty(Theta_t))
    % return the closest model to the current estimates
    theta_min = 1;
    theta_val = 1.0;
    for m=1:M
        v = sum(abs(model_hmu(m,:)'-arm_hmu));
        if (v < theta_val)
            theta_min = m;
            theta_val = v;
        end
    end
    Theta_t = theta_min;
end