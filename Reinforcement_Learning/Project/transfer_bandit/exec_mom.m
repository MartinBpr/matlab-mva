function [hrho, model_hmu, rho_conf, model_conf, exact_err, M2, M3] = exec_mom( setting, samples, M2, M3, n_tasks, use_exact_error )

%Inputs:

%%%%% dataC is a K*1 cell array where K is the number of arms
%%%each entry of the cell is a vector consisits of all the samples for each
%%%arm in the latest task

%%$ M2 latest estimate of the second moment : for the first time you need to initialize it with the K*K matrix of all 0

%%% M3 latest estimate of the third moment : for the first time you need to initialize it with the K*K*K array of all 0


%%% K  number of arms

%%% m number of models

%%% N number of tasks



%%% rho the true distributions on the models

%%% mumat the true average rewards for all the models and all the arms (We
%%% need these two (mu, rho) to artificially make the confidence intervals since
%%% we do not yet have thecomplete theoretical bounds )



%Outputs:

%%%%rhohat: Emprirical rho

%%%% muhat: Empirical mu for all the models and arms it is a true mpirical
%%%% estimation up to a permutation in m
%%% M2 new estimate of the second moment

%%% M3 new estimate of the third moment
%%% errMu maximum error per arm per model for the estimated average reward

%%% errRho maximum error per model for the esimated rho





C.K=setting.K;
C.m=setting.M;
C.L1=ceil(10*log(setting.M));
C.L=ceil(log(10)*setting.M^2);
C.mu=setting.mu';
C.rho=setting.rho';
%sampNum=8;
%NVect=logspace(1,sampNum/2,sampNum);
errMu=0;
errRho=0;
%N=size(dataC,1);


%for i=1:N
data=zeros(setting.K,3);
for j=1:setting.K
    csz=length( samples{j} );
    data(j,1)=mean(samples{j}(1:floor(csz/3)));
    data(j,2)=mean(samples{j}(floor(csz/3)+1:ceil(2*csz/3)));
    data(j,3)=mean(samples{j}(ceil(2*csz/3)+1:end));
end


[M2,M3]=estMomRec(data,M2,M3,C,n_tasks);


[WM,W,D]=whiten(M2,M3,C);
[v,lambda]=eigDecomp(WM,C);

rhohat=1./(lambda.^2);
rhohat=rhohat/sum(rhohat);
lambda=1./(rhohat.^0.5);

muhat=ones(C.K,1)*lambda'.*(pinv(W')*v);
rhohat=1./(lambda.^2);


model_hmu = muhat';
hrho = rhohat';
rho_conf = 0.0;

% reordering of the models
p = perms(1:setting.M);
n_perms = size(p,1);
dist_max = 100.0;
best_perm = 1;
for i=1:n_perms
    new_model_hmu = model_hmu(p(i,:),:);
    dist = max(max(abs(new_model_hmu - setting.mu)));
    if (dist < dist_max)
        dist_max = dist;
        best_perm = i;
    end
end

model_hmu = model_hmu(p(best_perm,:),:);

exact_err = dist_max;
if (false)%compute_exact_error == 1)
    model_conf = exact_err;
else
    model_conf = 4/sqrt(n_tasks);
end

%disp(['Exact error = ' num2str(dist_max)]);
