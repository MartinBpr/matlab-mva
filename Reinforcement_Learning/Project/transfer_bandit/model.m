classdef model
    
    properties
        n_arms
        rates
    end
    
    methods
        % X is the matrix of rates for the users in its cluster
        function self = model(X)
            [~, self.n_arms] = size(X);
            self.rates = {};
            
            for j = 1:self.n_arms 
                non_zeros = find(X(:,j));
                self.rates{j} = X(non_zeros,j);
            end
            
        end
        
        function [reward] = play(self, pulled_arm)
            rates_list = self.rates{pulled_arm};
            reward = rates_list(randi(length(rates_list)));
            %normalization
            reward = (reward - 1) / 4;
        end
        
        function [mu] = mu(self, arm)
           rates_list = self.rates{arm};
           mu = mean(rates_list);
           %normalization
           mu = (mu - 1) / 4;
        end
    end
    
end

