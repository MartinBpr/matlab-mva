function [complexity Delta_min] = compute_ucb_complexity(setting, btheta)

deltas = setting.Delta(btheta,:);
deltas(setting.optarm(btheta)) = [];
Delta_min = min(deltas);
complexity = sum(1./deltas);
