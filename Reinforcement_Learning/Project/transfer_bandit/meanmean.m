s = RandStream('mcg16807','Seed',0);
RandStream.setGlobalStream(s);

M=5;
mu=[0.1 0.2 0.3 0.4 0.5]';
%rho=1/M * ones(M,1);
rho=[0.96 0.01 0.01 0.01 0.01]';

mu_mean = mu'*rho;

J = 10;
T = 100;
runs=1000;
errs = zeros(runs, 1);

for r=1:runs
    tasks = discreternd(rho, J);
    Y = rand(J,T)<repmat(mu(tasks),1,T);
    errs(r) = abs(mu_mean - mean(mean(Y)));
end

mean(errs)