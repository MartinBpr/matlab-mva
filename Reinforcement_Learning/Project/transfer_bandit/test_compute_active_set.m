K = 5;
M = 3;

mu = zeros(M, K);
mu(1, 1) = 0.9;
mu(1, 2) = 0.8;
mu(1, 3) = 0.7;
mu(1, 4) = 0.1;
mu(1, 5) = 0.2;

mu(2, 1) = 0.89;
mu(2, 2) = 0.79;
mu(2, 3) = 0.71;
mu(2, 4) = 0.2;
mu(2, 5) = 0.1;

mu(3, 1) = 0.85;
mu(3, 2) = 0.88;
mu(3, 3) = 0.1;
mu(3, 4) = 0.05;
mu(3, 5) = 0.01;

hmu = [0.85 0.86 0.74 0.01 0.15]';
conf = [0.1 0.1 0.1 0.1 0.1]';

cur_time=cputime;
for i=1:10000
    Theta_t = compute_active_set(M, mu, hmu, conf);
end
elapsed_time = cputime-cur_time