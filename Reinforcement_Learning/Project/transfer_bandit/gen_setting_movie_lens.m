function s = gen_setting_movie_lens()


% number of arms
s.K = 4;
% number of models
s.M = 4;

filename = ['movielens_models_movies' num2str(s.K) '_clusters' num2str(s.M) '.mat'];

a = dir(pwd);
b=struct2cell(a);
if ((any(ismember(b(1,:),filename))))
    load(filename, 'models');
    disp(['models have been loaded from' filename])
else
    models = gen_movie_lens_models(s.K, s.M);
    save(filename, 'models');
end



s.mu = zeros(s.M, s.K);
for m = 1:s.M
   for k = 1:s.K
       s.sample{m, k} = @()models{m}.play(k);
       s.mu(m, k) = models{m}.mu(k);
   end
end



% distribution over models
%s.rho = [0.15 0.1 0.45 0.15 0.15];
s.rho = (1/s.M)*ones(1,s.M);

% compute the optimal arm and the optimal value for each model
s.optmu = zeros(s.M, 1);
s.optarm = zeros(s.M, 1);
for m=1:s.M
    % value and arm
    [s.optmu(m) s.optarm(m)] = max(s.mu(m,:));
end

% model gaps
s.Gamma = zeros(s.M,s.M,s.K);
for i=1:s.K
    for m=1:s.M
        for mm=1:s.M
            s.Gamma(m,mm,i) = abs(s.mu(m,i) - s.mu(mm,i));
        end
    end
end

% arm gaps
s.Delta = zeros(s.M,s.K);
for i=1:s.K
    for m=1:s.M
        s.Delta(m,i) = s.optmu(m,:) - s.mu(m,i);
    end
end

test_setting = false;
if test_setting
    mu_ = zeros(s.M, s.K);
    n_tests = 1000;
    for i = 1:n_tests
       for m = 1:s.M
           for k = 1:s.K
               mu_(m, k) = mu_(m, k) + s.sample{m, k}()/n_tests;
           end
       end
    end
    s.mu
    (s.mu - mu_) ./ s.mu
    figure();
    subplot(2,2,1);
    [c,x] = hist(models{1}.rates{3}, [1 2 3 4 5]);
    bar(x,c/length(models{1}.rates{3}));
    title('Cluster 2, movie 2 (worst movie)', 'Fontsize', 16);
    xlabel('rating', 'Fontsize', 14);
    ylabel('probability', 'Fontsize', 14);
    
    subplot(2,2,2);
    [c,x] = hist(models{1}.rates{1}, [1 2 3 4 5]);
    bar(x,c/length(models{2}.rates{3}));
    title('Cluster 1, movie 1', 'Fontsize', 16);
    xlabel('rating', 'Fontsize', 14);
    ylabel('probability', 'Fontsize', 14);
        
    subplot(2,2,3);
    [c,x] = hist(models{4}.rates{4}, [1 2 3 4 5]);
    bar(x,c/length(models{4}.rates{4}));
    title('Cluster 4, movie 4', 'Fontsize', 16);
    xlabel('rating', 'Fontsize', 14);
    ylabel('probability', 'Fontsize', 14);
    
    subplot(2,2,4);
    [c,x] = hist(models{3}.rates{4}, [1 2 3 4 5]);
    bar(x,c/length(models{3}.rates{4}));
    title('Cluster 3, movie 4 (best movie)', 'Fontsize', 16);
    xlabel('rating', 'Fontsize', 14);
    ylabel('probability', 'Fontsize', 14);
    

end


