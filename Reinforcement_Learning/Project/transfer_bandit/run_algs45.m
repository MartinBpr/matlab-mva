format compact;

algs = [4 5]
n_values = [2000]
settings = 2
J = 2000
runs = 1;

for s=settings
    for alg=algs
        for n=n_values
            for r=1:runs
                run_experiment(s, n, J, alg);
            end
        end
    end
end

