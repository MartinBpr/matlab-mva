function smoothed_results = smooth_results_mean(results, w)

J = length(results);

smoothed_results = zeros(1, J-w);

for j = 1:(J-w)
    window = results(j:(j+w));
    window = sort(window);
    smoothed_results(j) = mean(window);
end

%plot(1:(J-w), smoothed_results)