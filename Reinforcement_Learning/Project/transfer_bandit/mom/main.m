

%%%% muhat estimated average reward
%%%%  rhohat estimated model
%%%% Here the rewards are generated according to some gaussian distribution 
%%%% (in function SampGen) you can replace it with the rewards of your
%%%% choice just comment everything between 17 and 47 (except the for loop)
%%%% and put your codes bandit samples

rng(1000);
C.K=10;
C.m=3;
C.L=200;
C.L1=200;
sampNum=8;
NVect=logspace(1,sampNum/2,sampNum);
errMu=zeros(1,sampNum);
errRho=zeros(1,sampNum);
%N=10;
rho=rand(C.m,1);
C.rho=rho/sum(rho);

while min(C.rho)<1/(2*C.m)
rho=rand(C.m,1);
C.rho=rho/sum(rho);
end;

eps=0.5;
C.sig=0.5;
C.mu=rand(C.K,C.m);


while min(svd(C.mu))<eps
    C.mu=2*rand(C.K,C.m)-1;
end;


for c=1:sampNum
    
    
    data=SampGen(C,NVect(c));
    
    
    %%%%% data is a structure array with   NVect(c) (number of tasks) entries, 
    %%%%%%each entry is a 3\times K matrix consists of the first three samples for each arm 
    
    
    [M1,M2,M3]=estMom(data,C,NVect(c));
    
    
    %M2Hat=zeros(C.K,C.K);
    %M3Hat=zeros(C.K,C.K,C.K);
    
    
    
    %for i=1:C.m
    %muMat=C.mu(:,i)*C.mu(:,i)';
    
    
    %M2Hat=M2Hat+muMat*C.rho(i);
    
    %muRep2=repmat(muMat,[1,1,C.K]);
    %muRep3=repmat(reshape(C.mu(:,i),[1,1,C.K]),[C.K,C.K,1]);
    %M3Hat=M3Hat+muRep2.*muRep3*C.rho(i);
    %end;
    
    %for i=1:C.m
    
    %M2 = M2+data(i).tri(:,1)*data(i).tri(:,2)';
    %M2Rep=repmat(M2,[1,1,C.K]);
    %M3Rep=repmat(reshape(data(i).tri(:,3),[1,1,C.K]),[C.K,C.K,1]);
    %M3= M3+M2Rep.*M3Rep;
    %end;
    
    [WM,W,D]=whiten(M2,M3,C);
    
    %WMC=zeros(C.m,C.m,C.m);
    
    %for i=1:C.m
    %muhat=W'*C.mu(:,i);
    
    %muMat=muhat*muhat';
    
    %M2=M2+muMat*C.rho(i);
    
    %muRep2=repmat(muMat,[1,1,C.m]);
    %muRep3=repmat(reshape(muhat,[1,1,C.m]),[C.m,C.m,1]);
    %WMC=WMC+C.rho(i)*(muRep2.*muRep3);
    %end;
    
    
    
    
    [v,lambda]=eigDecomp(WM,C);
    
    
    muhat=ones(C.K,1)*lambda'.*(pinv(W')*v);
    rhohat=1./(lambda.^2);
    
    for k=1:C.m
        
        distMin=norm(muhat+C.mu);
        distMinRho=1;
        
        for j=1:C.m
            distMin=min(norm(muhat(:,j)-C.mu(:,k)),distMin);
            distMinRho=min(abs(1/rhohat(j)-C.rho(k)),distMinRho);
        end;
        errMu(c)=errMu(c)+distMin;
        errRho(c)=errRho(c)+distMinRho;
        
    end;
    
end;


return;








