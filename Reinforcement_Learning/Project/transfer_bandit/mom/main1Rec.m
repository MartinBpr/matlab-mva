

%%%% muhat estimated average reward
%%%%  rhohat estimated model
%%%% Here the rewards are generated according to some gaussian distribution 
%%%% (in function SampGen) you can replace it with the rewards of your
%%%% choice just comment everything between 17 and 47 (except the for loop)
%%%% and put your codes bandit samples

rng(1000);
C.K=6;
C.m=4;
%C.L=300;
%C.L1=300;
sampNum=1000;
N=1;
%NVect=logspace(1,sampNum/2,sampNum);
errMu=zeros(1,sampNum);
errRho=zeros(1,sampNum);
%N=10;
rho=rand(C.m,1);
C.rho=rho/sum(rho);

while min(C.rho)<1/(2*C.m)
rho=rand(C.m,1);
C.rho=rho/sum(rho);
end;

eps=0.5;
C.sig=0.5;
C.mu=rand(C.K,C.m);


while min(svd(C.mu))<eps
    C.mu=2*rand(C.K,C.m)-1;
end;

M2=zeros(C.K,C.K);
M3=zeros(C.K,C.K,C.K);


for c=1:sampNum
    
    
    
    data=SampGenRec(C,N);
    
    %%%%% data is a structure array with   NVect(c) (number of tasks) entries, 
    %%%%%%each entry is a 3\times K matrix consists of the first three samples for each arm 
    
    [muhat,rhohat,M2, M3,err1,err2]=MOMMainFuncRec(data,M2,M3,C.K,C.m,c,C.mu,C.rho);
    errMu(c)=err1;
    errRho(c)=err2;
    
end;


return;








