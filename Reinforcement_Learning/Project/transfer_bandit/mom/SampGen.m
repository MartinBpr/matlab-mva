% d=15;
% k=5;
% N=1000;
% rho=ones(k,1);
% rho=rho/sum(rho);
% eps=0.1;
% mu=rand(d,k);
% 
% while min(svd(mu))<eps
% mu=rand(d,k);
% end;

function data=SampGen(C,N)


for i=1:N
 rhoSum=cumsum(C.rho);
 r1=rand;
 ind=find(rhoSum>r1);
    for j=1:C.K
 
 %rhoInd=rhoSum(ind);
 Ind=ind(1);
     
data{i,j}=C.sig^0.5* randn(1,3)+C.mu(j,Ind);

    end;
end;

return;




