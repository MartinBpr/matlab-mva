function [  muhat, rhohat ,  M2, M3, errMu, errRho ] = MOMMainFuncRec( dataC, M2, M3, K, m, N, mumat, rho )



%Inputs:

%%%%% dataC is a K*1 cell array where K is the number of arms
%%%each entry of the cell is a vector consisits of all the samples for each
%%%arm in the latest task

%% M2 latest estimate of the second moment : for the first time you need to initialize it with the K*K matrix of all 0

%% M3 latest estimate of the third moment : for the first time you need to initialize it with the K*K*K array of all 0


%%% K  number of arms

%%% m number of models

%%% N number of tasks



%%% rho the true distributions on the models

%%% mumat the true average rewards for all the models and all the arms (We
%%% need these two (mu, rho) to artificially make the confidence intervals since
%%% we do not yet have thecomplete theoretical bounds )



%Outputs:

%%%%rhohat: Emprirical rho

%%%% muhat: Empirical mu for all the models and arms it is a true mpirical
%%%% estimation up to a permutation in m
%%% M2 new estimate of the second moment

%%% M3 new estimate of the third moment
%%% errMu maximum error per arm per model for the estimated average reward

%%% errRho maximum error per model for the esimated rho






C.K=K;
C.m=m;
C.L1=ceil(10*log(m));
C.L=ceil(log(10)*m^2);
C.mu=mumat;
C.rho=rho;
%sampNum=8;
%NVect=logspace(1,sampNum/2,sampNum);
errMu=0;
errRho=0;
%N=size(dataC,1);


%for i=1:N
    data=zeros(K,3);
    for j=1:K
        csz=length( dataC{j} );
        data(j,1)=mean(dataC{j}(1:floor(csz/3)));
        data(j,2)=mean(dataC{j}(floor(csz/3)+1:ceil(2*csz/3)));
        data(j,3)=mean(dataC{j}(ceil(2*csz/3)+1:end));
    end


[M2,M3]=estMomRec(data,M2,M3,C,N);


[WM,W,D]=whiten(M2,M3,C);
[v,lambda]=eigDecomp(WM,C);

rhohat=1./(lambda.^2);
rhohat=rhohat/sum(rhohat);
lambda=1./(rhohat.^0.5);

muhat=ones(C.K,1)*lambda'.*(pinv(W')*v);
rhohat=1./(lambda.^2);

for j=1:C.m
    
    distMin=norm(muhat+C.mu);
    distMinRho=1;
    
    for k=1:C.m
        distMin=min(max(abs((muhat(:,j)-C.mu(:,k)))),distMin);
        distMinRho=min(abs(rhohat(j)-C.rho(k)),distMinRho);
    end;
    errMu=max(errMu,distMin);
    errRho=max(errRho,distMinRho);
    
end;

return;




