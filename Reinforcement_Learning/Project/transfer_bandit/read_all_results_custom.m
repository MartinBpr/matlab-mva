directory = '/Users/Martin/Projects/matlab-mva/Reinforcement_Learning/Project/transfer_bandit/'

% horizon
%n_values = [500 1000 2000 5000 10000 20000];
%n_values = [1000];
%n_values = [2000];
n_values = [5000];

% setting
setting_idx = 3;

% list of the algoritms to run
algs = [1 2 3 4];
n_algs = length(algs);

% number of episodes
J = 5000;

avg_regret = zeros(n_algs, length(n_values), J);
stddev_regret = zeros(n_algs, length(n_values), J);
avg_exp_comp = zeros(n_algs, length(n_values), J);
tot_runs = zeros(n_algs, length(n_values));
for a = 1:n_algs
    for n = 1:length(n_values)
        [x1, x2, x3, x4, x5] = ... 
            read_results(algs(a), n_values(n), J, setting_idx, directory);
        %size(x1), size(x2), size(x3), size(x4)
        avg_regret(a, n, :) = x1;
        stddev_regret(a, n, :) = x2;
        avg_exp_comp(a, n, :) = x3; 
        tot_runs(a, n) = x5;
    end
end

tot_runs

avg_regret_ucb = mean(reshape(avg_regret(1,:,:), length(n_values), J), 2);
avg_regret_ucb_opt = mean(reshape(avg_regret(2,:,:), length(n_values), J), 2);
avg_regret_mucb = mean(reshape(avg_regret(3,:,:), length(n_values), J), 2);
regret_tucb = reshape(avg_regret(4,:,:), length(n_values), J);
avg_regret_tucb_1000 = mean(regret_tucb(:,800:1000), 2);
avg_regret_tucb_2000 = mean(regret_tucb(:,1600:2000), 2);
avg_regret_tucb = mean(regret_tucb(:,4000:5000), 2);


plot_comparison_n_all(n_values, [avg_regret_ucb avg_regret_ucb_opt avg_regret_mucb ...
    avg_regret_tucb_1000 avg_regret_tucb_2000 avg_regret_tucb]);


n_idx = length(n_values);

figure;
all_regrets = reshape(avg_regret(:,n_idx,:), n_algs, J);
plot(1:J, all_regrets, '.');
title('all regrets');

figure
C = num2cell(all_regrets,2);
w = 500;
all_regrets_smoothed_as_cell = cellfun(@(x)smooth_results_mean(x, w),C, 'UniformOutput', false);
all_regrets_smoothed = cell2mat(all_regrets_smoothed_as_cell);
plot(1:(J-w), all_regrets_smoothed);
title('all regrets smoothed');

%figure
%C = num2cell(all_regrets,2);
%w = 200; c = 10;
%all_regrets_smoothed_as_cell = cellfun(@(x)smooth_results_median(x, w, c),C, 'UniformOutput', false);
%all_regrets_smoothed = cell2mat(all_regrets_smoothed_as_cell);
%plot(1:(J-w), all_regrets_smoothed);
%title('all regrets smoothed new median');

%figure
avg_complexity_ucb = mean(reshape(avg_exp_comp(1,:,:), length(n_values), J), 2);
avg_complexity_ucb_opt = mean(reshape(avg_exp_comp(2,:,:), length(n_values), J), 2);
avg_complexity_mucb = mean(reshape(avg_exp_comp(3,:,:), length(n_values), J), 2);
complexity_tucb = reshape(avg_exp_comp(4,:,:), length(n_values), J);

complexities_comparison = [avg_complexity_ucb(n_idx,:)*ones(1,J); avg_complexity_ucb_opt(n_idx,:)*ones(1,J); avg_complexity_mucb(n_idx,:)*ones(1,J); complexity_tucb];
sub_sample = [1:500:J];
plot_complexity_comparison(sub_sample, complexities_comparison(:, sub_sample));
%plot(1:J, all_complexities);
%title('complexities');

regret_comparison = [avg_regret_ucb(n_idx,:)*ones(1,J-w); avg_regret_ucb_opt(n_idx,:)*ones(1,J-w); avg_regret_mucb(n_idx,:)*ones(1,J-w); all_regrets_smoothed(4,:)];
sub_sample = [1:500:(J-w)];
plot_episode_regret(500:500:(J-1), regret_comparison(:, sub_sample));
