% number of arms
s.K = 5;

% number of models
s.M = 3;

s.mu = zeros(s.M, s.K);

% model 1
s.mu(1, 1) = 0.9;
s.mu(1, 2) = 0.8;
s.mu(1, 3) = 0.7;
s.mu(1, 4) = 0.1;
s.mu(1, 5) = 0.2;

% model 2
s.mu(2, 1) = 0.89;
s.mu(2, 2) = 0.79;
s.mu(2, 3) = 0.71;
s.mu(2, 4) = 0.2;
s.mu(2, 5) = 0.1;

% model 3
s.mu(3, 1) = 0.85;
s.mu(3, 2) = 0.88;
s.mu(3, 3) = 0.1;
s.mu(3, 4) = 0.05;
s.mu(3, 5) = 0.01;