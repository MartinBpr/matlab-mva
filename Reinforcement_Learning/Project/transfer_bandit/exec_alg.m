function [pulls all_samples] = exec_alg(setting, alg, n, model_hmu, model_conf, btheta, j, J)

pulls = zeros(setting.K, 1);
cumulX = zeros(setting.K, 1);
arm_hmu = zeros(setting.K, 1);

% all the samples observed by the bandit algorithm per arm
all_samples = {};
for i=1:setting.K
    all_samples{i} = [];
end

%c = 1.0*sqrt(log(n)/2);
c = sqrt(2*log(n));
%c2 = 4*log(n);

min_pulls = 9;

non_optimal_arms = [];
for i=1:setting.K
    if (isempty(find(setting.optarm == i)))
        non_optimal_arms = [non_optimal_arms i];
    end
end

for t=1:n
    arm_conf = c*sqrt(1./pulls);
    
    if (t<=min_pulls*setting.K)
        best_arm = mod(t, setting.K)+1;
        
    elseif (alg == 1) 
    %% UCB
        B = arm_hmu + arm_conf;
        [best_value best_arm] = max(B);

    elseif (alg == 2)
    %% UCB-opt
        B = arm_hmu + arm_conf;
        B(non_optimal_arms) = 0.0;
        [best_value best_arm] = max(B);

    elseif (alg == 3) 
    %% mUCB
        % compute the active set
        Theta_t = compute_active_set(setting.M, model_hmu, arm_hmu, arm_conf);
        
        % compute the optimistic model
        [best_value best_theta_idx] = max(setting.optmu(Theta_t));
        best_arm = setting.optarm(Theta_t(best_theta_idx));
        
    elseif (alg == 4) 
    %% umUCB
        % compute the active set
        Theta_t = compute_uncertain_active_set(setting.M, model_hmu, model_conf, arm_hmu, arm_conf);
        
        % for each active model truncate its optimistic estimates
        Bvalue = zeros(setting.M, setting.K);
        for m=Theta_t
            Bvalue(m,:) = min(model_hmu(m,:)'+model_conf, arm_hmu+arm_conf);
        end
        [bestBvalue best_arms] = max(Bvalue, [], 2);
        [best_value best_model] = max(bestBvalue);
        best_arm = best_arms(best_model); 
        
%         best_value = max(max(Bvalue));
%         optimal_arms = [];
%         for m=Theta_t
%             optimal_arms = [optimal_arms, find(best_value+0.00001>Bvalue(m,:) & Bvalue(m,:)+0.0001>best_value)];
%         end
%         best_arm= optimal_arms(unidrnd(length(optimal_arms)));
    elseif (alg == 5)
        %% umUCB+
        %unif_pulls = n*(J-j)/J;
        unif_pulls = n/sqrt(j);
        if (t<=unif_pulls)
            best_arm = mod(t, setting.K)+1;
        else
            % compute the active set
            Theta_t = compute_uncertain_active_set(setting.M, model_hmu, model_conf, arm_hmu, arm_conf);
            
            % for each active model truncate its optimistic estimates
            Bvalue = zeros(length(Theta_t), setting.K);
            for m=Theta_t
                Bvalue(m,:) = min(model_hmu(m,:)'+model_conf, arm_hmu+arm_conf);
            end
            [bestBvalue best_arms] = max(Bvalue, [], 2);
            [best_value best_model] = max(bestBvalue);
            best_arm = best_arms(best_model);
        end
    elseif (alg == 6)
        %% random
        best_arm = mod(t, setting.K)+1;
    end
    
    %% generate a sample from the true model and the chosen arm
    if isfield(setting, 'sample')
        sample = setting.sample{btheta, best_arm}();
    else
        sample = (rand < setting.mu(btheta, best_arm));
    end
    cumulX(best_arm) = cumulX(best_arm) + sample;
    pulls(best_arm) = pulls(best_arm)+1;
    arm_hmu(best_arm) = cumulX(best_arm)/pulls(best_arm);
    
    all_samples{best_arm} = [all_samples{best_arm} sample];
end

% fig = figure('XVisual','');
% Theta_t = compute_active_set(setting.M, model_hmu, arm_hmu, arm_conf);
% plot_arms(fig, setting.M, setting.K, setting.mu, btheta, arm_hmu, pulls, arm_conf, model_hmu, model_conf, Theta_t);

