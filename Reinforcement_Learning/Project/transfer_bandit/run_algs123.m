format compact;

algs = [1 2 3]
n_values = [1000 2000 5000 10000]
n_values = [15000]
settings = 2
J = 20
runs = 20;

for s=settings
    parfor alg=algs
        for n=n_values
            for r=1:runs
                run_experiment(s, n, J, alg, 0);
            end
        end
    end
end

