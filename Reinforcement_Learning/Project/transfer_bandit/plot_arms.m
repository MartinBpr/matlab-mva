function plot_arms(figure1, M, K, mu, btheta, hmu, pulls, eps_i, hmodel_mu, eps_j, active_set)

clf;

% Create axes
n_ticks = 1;
arm_ticks_pos = 1:K;
model_ticks_pos = zeros(M,K);
all_ticks_pos = 0;
for i=1:K
    ticks_labels{n_ticks} = ['A' num2str(i) ' (' num2str(pulls(i)) ')'];
    all_ticks_pos(n_ticks) = arm_ticks_pos(i);
    n_ticks = n_ticks+1;
    for m=1:M
        ticks_labels{n_ticks} = ['m' num2str(m)];
        model_ticks_pos(m,i) = i+m/(M+3);
        all_ticks_pos(n_ticks) = i+m/(M+3);
        n_ticks = n_ticks+1;
    end
end

axes1 = axes('Parent',figure1,'XTickLabel',ticks_labels,'XTick',all_ticks_pos,'FontSize',10);
box(axes1,'on');
hold(axes1,'all');
min_arm = min(hmu-eps_i)-0.1;
min_model = min(hmodel_mu(:)-eps_j)-0.1;
max_arm = max(hmu+eps_i)+0.1;
max_model = max(hmodel_mu(:)+eps_j)+0.1;
ylim(axes1,[max(min(min_arm,min_model),-0.5) min(max(max_model,max_arm),1.5)]);
xlim(axes1,[0.5 K+1.0]);

% Create xlabel
xlabel('Arms-Models','FontSize',16);

% Create ylabel
ylabel('Reward','FontSize',16);

% Create confidence intervals for arms
errorbar(arm_ticks_pos,hmu,eps_i,'MarkerSize',8,'Marker','square','LineStyle','none',...
    'LineWidth',1,...
    'Color',[0 0 1]);

% Create confidence intervals and true means for models
for m=1:M
    if m==btheta
        errorbar(model_ticks_pos(m,:),hmodel_mu(m,:),eps_j.*ones(1,K),'MarkerSize',8,'Marker','square','LineStyle','none',...
            'LineWidth',1.5,...
            'Color',[0 1 0]);
    
        plot(model_ticks_pos(m,:),mu(m,:),'MarkerSize',7,'Marker','o','LineWidth',2,'LineStyle','none',...
            'Color',[0 1 0]);

    elseif any(active_set == m)
        errorbar(model_ticks_pos(m,:),hmodel_mu(m,:),eps_j.*ones(1,K),'MarkerSize',8,'Marker','square','LineStyle','none',...
            'LineWidth',1,...
            'Color',[0 1 0]);
        plot(model_ticks_pos(m,:),mu(m,:),'MarkerSize',7,'Marker','o','LineWidth',2,'LineStyle','none',...
            'Color',[0 1 0]);
    else
        errorbar(model_ticks_pos(m,:),hmodel_mu(m,:),eps_j.*ones(1,K),'MarkerSize',8,'Marker','square','LineStyle','none',...
            'LineWidth',1,...
            'Color',[1 0 0]);
        plot(model_ticks_pos(m,:),mu(m,:),'MarkerSize',7,'Marker','o','LineWidth',2,'LineStyle','none',...
            'Color',[1 0 0]);        
    end
end

% Create true arm means
plot(arm_ticks_pos,mu(btheta,:),'MarkerSize',7,'Marker','o','LineWidth',2,'LineStyle','none',...
    'Color',[0 0 1]);


drawnow;

