function smoothed_results = smooth_results_median(results, w, c)

J = length(results);

smoothed_results = zeros(1, J-w);

for j = 1:(J-w)
    window = results(j:(j+w));
    window = sort(window);
    %smoothed_results(j) = median(window);
    center = ((-c/2):(c/2)) + w/2;
    smoothed_results(j) = mean(window(center));
end

%plot(1:(J-w), smoothed_results)