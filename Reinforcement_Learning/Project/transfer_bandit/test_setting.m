function test_setting(setting_idx)

format compact;

if (setting_idx == 1)
    s = gen_setting1();
elseif (setting_idx == 2)
    s = gen_setting2();
elseif (setting_idx == 3)
    s = gen_setting_movie_lens();
end

disp('\hline');
header = ' ';
for i=1:s.K
    header = [header '& Arm' num2str(i) ' '];
end
header = [header '\\'];
disp(header);
disp('\hline');
for m=1:s.M
    line = ['$\theta_' num2str(m) '$ '];
    for i=1:s.K
        line = [line ' & ' num2str(s.mu(m,i)) ' '];
    end
    line = [line '\\'];
    disp(line);
end
disp('\hline');
disp('\hline');
disp(' ');

fig = figure()%'XVisual','');
plot_models(fig, s.M, s.K, s.mu, s.optarm);

ucb_exp_comp  = 0.0;
ucb_opt_exp_comp = 0.0;
mucb_exp_comp = 0.0;

complexities = zeros(3, s.M);

disp('\hline');
disp('& \textit{UCB} & \textit{UCB+} & \textit{mUCB} \\');
disp('\hline');
for m=1:s.M
    [ucb_comp Delta_min] = compute_ucb_complexity(s, m);
    ucb_exp_comp = ucb_exp_comp + ucb_comp*s.rho(m);
    
    [ucb_opt_comp opt_Delta_min] = compute_ucb_opt_complexity(s, m);
    ucb_opt_exp_comp = ucb_opt_exp_comp + ucb_opt_comp*s.rho(m);

    [mucb_comp Gamma_min Theta_opt] = compute_mucb_complexity(s, m);
    mucb_exp_comp = mucb_exp_comp + mucb_comp*s.rho(m);
    
    complexities(1, m) = ucb_comp;
    complexities(2, m) = ucb_opt_comp;
    complexities(3, m) = mucb_comp;

%     disp(['** Complexity of the problem (' num2str(m) ') **']);
%     disp(['UCB(c; d_min)             = ' num2str(ucb_comp) '; ' num2str(Delta_min)]);
%     disp(['UCB+(c; d_min)            = ' num2str(ucb_opt_comp) '; ' num2str(opt_Delta_min)]);
%     disp(['mUCB(c; |Theta+|; G_min)  = ' num2str(mucb_comp) '; ' num2str(length(Theta_opt)) '; ' num2str(Gamma_min)]);
    disp(['$\theta_' num2str(m) '$ & ' num2str(ucb_comp) ' & ' num2str(ucb_opt_comp) ' & ' num2str(mucb_comp) '\\']);
end
disp('\hline');
disp('\hline');
disp(['avg & ' num2str(ucb_exp_comp) ' & ' num2str(ucb_opt_exp_comp) ' & ' num2str(mucb_exp_comp) '\\']);
disp('\hline');
% disp('** Expected complexity of the problem **');
% disp(['UCB    = ' num2str(ucb_exp_comp)]);
% disp(['UCB+   = ' num2str(ucb_opt_exp_comp)]);
% disp(['mUCB   = ' num2str(mucb_exp_comp)]);

disp('Matrix complexity');
disp(svd(s.mu)');

disp('Second moment');
M2 = zeros(s.K, s.K);
for m=1:s.M
    M2 = M2 + s.rho(m).*kron(s.mu(m,:), s.mu(m,:)');
end
disp(svd(M2)');

disp('Testing umUCB complexity');
errs=fliplr([0.0001:0.00025:0.3]);
exp_comp = zeros(length(errs),1);
for i=1:length(errs)
    for m=1:s.M
        exp_comp(i) = exp_comp(i) + s.rho(m)*compute_umucb_complexity(s, s.mu, errs(i), m);
    end
    %disp([num2str(e) '  umUCB   = ' num2str(exp_comp)]);
end
plot_transfer_complexity(0.5.*errs, [ucb_exp_comp*ones(length(errs),1) mucb_exp_comp*ones(length(errs),1) exp_comp]);
