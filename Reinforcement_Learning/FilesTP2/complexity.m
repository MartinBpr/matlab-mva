function [c] = complexity(MAB)
    K = length(MAB);
    means = zeros(K,1);
    KL = zeros(K,1);
    for i = 1:K 
        arm = MAB{i};
        means(i) = arm.mean;
    end
    
    mu_star = max(means);
    for i = 1:K
        x = means(i);
        y = mu_star;
        KL(i) = x * log(x/y) + (1-x) * log((1-x)/(1-y));
    end
    
    c = 0;
    for i = 1:K
       if (KL(i) ~= 0)
           c = c + means(i)/KL(i);
       end
    end
end

