function [rew, draws] = naive(T,MAB)
K = length(MAB);
S = zeros(1,K);
N = zeros(1,K);

rew = zeros(1,T);
draws = zeros(1,T);

B = ones(1,K) * Inf;

for t = 1:T
    [~, ind_max] = max(B);
    
    chosen_arm = MAB{ind_max};
    reward = chosen_arm.play;
    rew(t) = chosen_arm.mean;
    draws(t) = ind_max;
    
    S(ind_max) = S(ind_max) + reward;
    N(ind_max) = N(ind_max) + 1;
    
    B(ind_max) = S(ind_max)/N(ind_max);
end

end

