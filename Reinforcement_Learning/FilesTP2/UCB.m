function [rew,draws] = UCB(T,alpha,MAB)
K = length(MAB);
N = zeros(1,K);
S = zeros(1,K);

rew = zeros(1,T);
draws = zeros(1,T);

B = ones(1,K) * Inf;

for t = 1:T
    [~, ind_max] = max(B);
    
    chosen_arm = MAB{ind_max};
    reward = chosen_arm.play;
    rew(t) = chosen_arm.mean;
    draws(t) = ind_max;
    
    S(ind_max) = S(ind_max) + reward;
    N(ind_max) = N(ind_max) + 1;
    
    for k = 1:K
        if N(k) > 0
            B(k) = S(k)/N(k) + sqrt(alpha * log(t) / N(k));
        end
    end
end

end

