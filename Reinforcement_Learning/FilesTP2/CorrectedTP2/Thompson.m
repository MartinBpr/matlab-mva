function [rec,tir] = Thompson(n,Problem)
% n longueur d'une trajectoire
% alpha : paramètre de l'UCB

K=length(Problem); % nb de bras

rec=zeros(1,n);% récompenses séquentiellement obtenues 
tir=zeros(1,n);% bras séquentiellement tirés
T=ones(1,K);% nombre courant de tirages de chaque bras
S=zeros(1,K);% récompense cumulée courante pour chaque bras


% Tirages successifs
for i=1:n
    % choix de l'action, observation de la récompense
    index=betarnd(S +1,T-S+1,1,K);
    m=max(index);
    I = find(index==m);
    a = I(floor(rand*length(I)+1));
    x=Problem{a}.play();
    % update
    rec(i)=x;
    tir(i)=a;
    S(a)=S(a)+x;
    T(a)=T(a)+1;    
end


end