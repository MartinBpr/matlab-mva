%% Build your own bandit problem 

% please change the parameters or arms!
Arm1=armBernoulli(0.5);
Arm2=armBeta(3,7);
Arm3=armExp(3);
Arm4=armFinite([0.1 0.3 0.7 0.8],[0.2 0.4 0.1 0.3]);

Bandit={Arm1,Arm2,Arm3,Arm4};
% bandit : set of arms

NbArms=length(Bandit);

Means=zeros(1,NbArms);
for i=1:NbArms
    Means(i)=Bandit{i}.mean;
end

% Display the means of your bandit (to find the best)
Means

mumax=max(Means);

%% Comparison of the regret on one run of the bandit algorithm

n=5000; % horizon
alpha=1;

[rew1,draws1]=naive(n,Bandit);
reg1=(1:n)*mumax-cumsum(rew1);
[rew2,draws2]=UCB(n,alpha,Bandit);
reg2=(1:n)*mumax-cumsum(rew2);

plot(1:n,reg1,1:n,reg2)


%% Mean and distribution of the regret 

Nsimu=1000;

Reg1=zeros(Nsimu,n);
Reg2=zeros(Nsimu,n);
Reg3=zeros(Nsimu,n);
Reg4=zeros(Nsimu,n);

for k=1:Nsimu
    [rec1,tir1]=naive(n,Bandit);
    Reg1(k,:)=(1:n)*mumax-cumsum(rec1);
    [rec2,tir2]=UCB(n,0.25,Bandit);
    Reg2(k,:)=(1:n)*mumax-cumsum(rec2);
    [rec3,tir3]=UCB(n,0.5,Bandit);
    Reg3(k,:)=(1:n)*mumax-cumsum(rec3);
    [rec4,tir4]=UCB(n,1,Bandit);
    Reg4(k,:)=(1:n)*mumax-cumsum(rec4);
end

% moyenne
reg1=mean(Reg1,1);
reg2=mean(Reg2,1);
reg3=mean(Reg3,1);
reg4=mean(Reg4,1);

plot(1:n,reg1,1:n,reg2,1:n,reg3,1:n,reg4)
legend({'naive','alpha=0.25','alpha=0.5','alpha=1'});

%% Empirical distribution of Rn

rdist1=Reg1(:,n);
rdist2=Reg2(:,n);
rdist3=Reg3(:,n);

subplot(3,1,1)
h=histc(rdist1,0:5:300);
bar(0:5:300,h);
subplot(3,1,2)
h=histc(rdist2,0:5:300);
bar(0:5:300,h);
subplot(3,1,3)
h=histc(rdist3,0:5:300);
bar(0:5:300,h);



%% Easy Problem


%% Difficult problem 


%% UCB versus Thompson Sampling

Arm1=armBernoulli(0.5);
Arm2=armBernoulli(0.7);
Arm3=armBernoulli(0.4);

BanditT={Arm1,Arm2,Arm3};

[rec,tir] = Thompson(n,BanditT);
[rec2,tir2] = UCB(n,0.5,BanditT);

reg=(1:n)*0.7-cumsum(rec);
reg2=(1:n)*0.7-cumsum(rec2);

plot(1:n,reg,1:n,reg2)
legend({'Thompson','UCB'})


%% Mean regret for Thompson and UCB

Nsimu=1000;

Reg1=zeros(Nsimu,n);
Reg2=zeros(Nsimu,n);


for k=1:Nsimu
    [rec1,tir1]=UCB(n,0.5,BanditT);
    Reg1(k,:)=(1:n)*0.7-cumsum(rec1);
    [rec2,tir2]=Thompson(n,BanditT);
    Reg2(k,:)=(1:n)*0.7-cumsum(rec2);
end

% moyenne
reg1=mean(Reg1,1);
reg2=mean(Reg2,1);

plot(1:n,reg1,1:n,reg2,1:n,reg3,1:n,reg4)
legend({'UCB','Thompson'});
