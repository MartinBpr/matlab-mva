function [rec,tir] = UCB (n,alpha,Problem)
% n longueur d'une trajectoire
% alpha : paramètre de l'UCB

K=length(Problem); % nb de bras

rec=zeros(1,n);% récompenses séquentiellement obtenues 
tir=zeros(1,n);% bras séquentiellement tirés
T=ones(K,1);% nombre courant de tirages de chaque bras
S=zeros(K,1);% récompense cumulée courante pour chaque bras

% Initialisation
for i=1:K
    S(i)=Problem{i}.play();
end


% Tirages successifs
for i=1:n
    % choix de l'action, observation de la récompense
    [m,a]=max(S./T+sqrt(alpha*log(i)./T));
    x=Problem{a}.play();
    % update
    rec(i)=x;
    tir(i)=a;
    S(a)=S(a)+x;
    T(a)=T(a)+1;    
end


end