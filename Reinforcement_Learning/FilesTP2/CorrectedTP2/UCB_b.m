function [rec,tir] = UCB_b (n,alpha,p1,p2,p3,p4)
% n longueur d'une trajectoire
% paramètres du problème (4 bras différents)
% alpha : paramètre de l'UCB


rec=zeros(1,n);% récompenses séquentiellement obtenues 
tir=zeros(1,n);% bras séquentiellement tirés
T=ones(4,1);% nombre courant de tirages de chaque bras
S=zeros(4,1);% récompense cumulée courante pour chaque bras

% Initialisation
for i=1:4
    S(i)=generate_b(i,p1,p2,p3,p4);
end


% Tirages successifs
for i=1:n
    % choix de l'action, observation de la récompense
    [m,a]=max(S./T+sqrt(alpha*log(i)./T));
    x=generate_b(a,p1,p2,p3,p4);
    % update
    rec(i)=x;
    tir(i)=a;
    S(a)=S(a)+x;
    T(a)=T(a)+1;    
end


end