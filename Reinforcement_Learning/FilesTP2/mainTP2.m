%% Build your own bandit problem 

% this is an example, please change the parameters or arms!
%{
Arm1=armBernoulli(0.5);
Arm2=armBeta(3,7);
Arm3=armExp(3);
Arm4=armFinite([0.1 0.3 0.7 0.8],[0.2 0.4 0.1 0.3]);

MAB={Arm1,Arm2,Arm3,Arm4};
%}
Arm1=armBernoulli(0.3);
Arm2=armBeta(7,5);
Arm3=armExp(3);
Arm4=armFinite([0.2 0.5 0.7 0.3],[0.6 0.2 0.1 0.1]);

MAB={Arm1,Arm2,Arm3,Arm4};

NbArms=length(MAB);

Means=zeros(1,NbArms);
for i=1:NbArms
    Means(i)=MAB{i}.mean;
end

% Display the means of your bandit (to find the best)
Means


%% Comparison of the regret on one run of the bandit algorithm
%{
n_simu = 100;
n=1000;
alpha= 0.02;

mean_reg1 = zeros(1,n+1);
mean_reg2 = zeros(1,n+1);

for i = 1:n_simu
    [rew1,draws1]=UCB(n,alpha,MAB);
    reg1=regret(MAB,rew1);
    mean_reg1 = (i-1)/i * mean_reg1 + 1/i * reg1;
    
    [rew2,draws2]=naive(n,MAB);
    reg2=regret(MAB,rew2);
    mean_reg2 = (i-1)/i * mean_reg2 + 1/i * reg2;
end

plot(1:n+1,mean_reg1,1:n+1,mean_reg2)
legend('UCB','naive');
%}
%% alpha test
%{

%n_alpha = 10;
%max_alpha = 0.010;
n_simu = 1000;
n=1000;

alphas = [0.000078125,0.0003125,0.00125,0.005,0.02,0.08,0.32];
n_alpha = length(alphas);

figure();
col=hsv(n_alpha + 1);

final_regret = zeros(1,n_alpha);

subplot(1,2,1);
hold on;
for k = 1:n_alpha
    alpha = alphas(k);%k / n_alpha * max_alpha;
    alpha
    mean_reg1 = zeros(1,n+1);
    for i = 1:n_simu
        [rew1,draws1]=UCB(n,alpha,MAB);
        reg1=regret(MAB,rew1);
        mean_reg1 = (i-1)/i * mean_reg1 + 1/i * reg1;
    end
    
    final_regret(k) = mean_reg1(length(mean_reg1));
    plot(1:n+1,mean_reg1,'color',col(k,:), 'DisplayName',strcat('UCB : ', num2str(alpha)))
end

mean_reg2 = zeros(1,n+1);
for i = 1:n_simu    
    [rew2,draws2]=naive(n,MAB);
    reg2=regret(MAB,rew2);
    mean_reg2 = (i-1)/i * mean_reg2 + 1/i * reg2;
end
plot(1:n+1,mean_reg2,'color',col(length(col),:), 'DisplayName','naive','LineWidth',3)


legend('-DynamicLegend');
hold off;
subplot(1,2,2);
%plot((1:n_alpha)/n_alpha * max_alpha,final_regret)
semilogx(alphas,final_regret)
%figure

%}
%% Easy Problem

subplot(1,2,1)
Arm1=armBernoulli(0.5);
Arm2=armBernoulli(0.3);
Arm3=armBernoulli(0.25);
%Arm4=armBernoulli(0.15);

%MAB={Arm1,Arm2,Arm3,Arm4};
MAB={Arm1,Arm2,Arm3};
c = complexity(MAB)

n_simu = 100;
n=10000;
alpha= 0.2;

mean_reg1 = zeros(1,n+1);
mean_reg2 = zeros(1,n+1);

for i = 1:n_simu
    [rew1,draws1]=UCB(n,alpha,MAB);
    reg1=regret(MAB,rew1);
    mean_reg1 = (i-1)/i * mean_reg1 + 1/i * reg1;
    
    [rew2,draws2]=Thompson(n,MAB);
    reg2=regret(MAB,rew2);
    mean_reg2 = (i-1)/i * mean_reg2 + 1/i * reg2;
end

plot(1:n+1,mean_reg1,1:n+1,mean_reg2,1:n+1, c*log(1:n+1))
legend('UCB','Thomson','Lai and Robbins lower bound');
title('Easy problem');



%% Difficult problem 


subplot(1,2,2)

Arm1=armBernoulli(0.43);
Arm2=armBernoulli(0.45);
Arm3=armBernoulli(0.50);
Arm4=armBernoulli(0.4);

MAB={Arm1,Arm2,Arm3,Arm4};
c = complexity(MAB)

n_simu = 100;
n=10000;
alpha= 0.02;

mean_reg1 = zeros(1,n+1);
mean_reg2 = zeros(1,n+1);

for i = 1:n_simu
    [rew1,draws1]=UCB(n,alpha,MAB);
    reg1=regret(MAB,rew1);
    mean_reg1 = (i-1)/i * mean_reg1 + 1/i * reg1;
    
    [rew2,draws2]=Thompson(n,MAB);
    reg2=regret(MAB,rew2);
    mean_reg2 = (i-1)/i * mean_reg2 + 1/i * reg2;
end

plot(1:n+1,mean_reg1,1:n+1,mean_reg2,1:n+1, c*log(1:n+1))
legend('UCB','Thomson','Lai and Robbins lower bound');
title('Complex problem');

%% UCB versus Thompson Sampling

