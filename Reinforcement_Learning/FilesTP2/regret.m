function [reg] = regret(MAB,rew)
K = length(MAB);
T = length(rew);

means = zeros(K,1);
for i = 1:K 
    arm = MAB{i};
    means(i) = arm.mean;
end

mu_star = max(means);

reg = zeros(1,T+1);
for t = 1:T
   reg(t+1) = reg(t) + mu_star - rew(t);
end

end

