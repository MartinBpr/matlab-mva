function [rew,draws] = Thompson(T,MAB)
K = length(MAB);
N = zeros(1,K);
S = zeros(1,K);

rew = zeros(1,T);
draws = zeros(1,T);

B = zeros(1,K);

for t = 1:T
    for i = 1:K
        B(i) = betarnd(S(i) + 1, N(i) - S(i) + 1);
    end
    
    [~, ind_max] = max(B);
    
    chosen_arm = MAB{ind_max};
    reward = chosen_arm.play;
    rew(t) = chosen_arm.mean;
    draws(t) = ind_max;
    
    S(ind_max) = S(ind_max) + reward;
    N(ind_max) = N(ind_max) + 1;
    
end

end

