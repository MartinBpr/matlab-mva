%% Problem setting 

M = 15; % taille du stock
K = 0.8; % frais de livraison
c = 0.5; % prix d'achat
h = 0.3; % co?t d'entretien
pr = 1; % prix de vente
gamma = 0.98; % calcul? ? partir du taux d'inflation

p = 0.1; % param?tre de la loi g?om?trique
D=zeros(1,M+1); % vecteur de la loi g?om?trique (tronqu?e)
D(1+(0:(M-1)))=p*(1-p).^(0:(M-1));
D(M+1)=1-sum(D);

x0=M; % stock initial

%% Visualisation d'une trajectoire

n=10;
pi=10*ones(1,M+1);%exemple de politique

[X1,R1] = trajectory(n,x0,pi,D,M,K,h,c,pr);

pi = [9,8,7,0,0,0,0,0,0,0,0,0,0,0,0,0];
[X2,R2] = trajectory(n,x0,pi,D,M,K,h,c,pr);

%plot((1:n)-1,cumsum(gamma.^((1:n)-1).*R1),(1:n)-1,cumsum(gamma.^((1:n)-1).*R2))
%legend('pi10', 'pi_ star')

% Estimation du la r?compense cumul?e discount?e


%% Calcul des param?tres du MDP

[P,R]=MDP(D,M,K,h,c,pr);

P(5,1:16,8);
R(5,1:16);

V = pol_eval1(P,R,pi,gamma,M);
%V


%% Value iteration

nb_it=1000;
V0=zeros(M+1,1);
[V, pol]=IV(P,R,gamma,V0,nb_it,M);
%V
%pol


%% Policy iteration
nb_it=100;
pi0=ones(M+1,1);
[V, pol]=PI(P,R,gamma,pi0,nb_it,M);
%V
%pol

%% Convergence comparaison
%{
V_star = V;
n_max = 400;

V0=zeros(M+1,1);
V_iter=zeros(n_max,1);
Vk = V0;
t = cputime;
for k =1:n_max
    [Vk, ~]=IV(P,R,gamma,Vk,1,M);
    V_iter(k) = norm(Vk - V_star, inf);
end
e_vi = cputime-t

%subplot(1,2,1)
plot(V_iter)

title('Value Iteration','FontSize',20)
xlabel('number of iterations','FontSize',15)
ylabel('$\|V^{*}-V_k\|_{\infty}$','Interpreter','LaTex','FontSize',20);

n_max = 400;
pi0=ones(M+1,1);
P_iter=zeros(n_max,1);
piK = pi0;

t = cputime;

for k =1:n_max
    [V_pik, piK]=PI(P,R,gamma,piK,1,M);
    P_iter(k) = norm(V_pik - V_star, inf);
end
e_pi = cputime-t

%subplot(1,2,2)
plot(P_iter)
title('Policy Iteration','FontSize',20)
xlabel('number of iterations','FontSize',15)
ylabel('$\|V^{*}-V_k\|_{\infty}$','Interpreter','LaTex','FontSize',20);
%plot(V_star)
V_star
diff(V_star)
%{
%plot(diff(V_star))
%plot(diff(diff(V_star)))
%diff(V_star)
%1 - cumsum(D)
%}
%}

%% Q-Learning

max_length_trajectory = 200;
n_trajectories = 3000;
pol
pol = ones(M+1,1) * 10;
V = pol_eval1(P,R,pol,gamma,M);

%{
[V1, plot1] = TD1(pol,n_trajectories,max_length_trajectory,D,M,K,h,c,pr,gamma);
%V1

[V2, plot2] =TD0(pol,n_trajectories,max_length_trajectory,D,M,K,h,c,pr,gamma);
%V2

[V_lambda, plot_lambda] =TD_lambda(0.5,pol,n_trajectories,max_length_trajectory,D,M,K,h,c,pr,gamma);
%V_lambda
%transpose(V)


subplot(1,2,1)
plot(1:length(plot1),plot1,1:length(plot2),plot2,1:length(plot_lambda) ,plot_lambda, [1,length(plot_lambda)], [V(1),V(1)])
subplot(1,2,2)
plot(1:length(V1),V1,1:length(V2),V2,1:length(V_lambda) ,V_lambda, 1:length(V), V)
legend('V - TD(1)', 'V - TD(0)', 'V - TD(lambda = 0.5)', 'V star')
pol
%}

tic
[Q, pol_] = Q_Learning_correction(1000000,D,M,K,h,c,pr,gamma);
pol_
toc

