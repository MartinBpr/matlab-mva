function [V, plot] = TD1(pi,n_trajectory,max_length_trajectory,D,M,K,h,c,pr,gamma)
%TD0 Computation of the expectation of V thanks to TD1 algorithm
%

V = ones(1,M+1)*1;
count_x = ones(1,M+1);
plot = [];
for n=1:n_trajectory
    trajectory_length = randi([10,max_length_trajectory]);
    x0 = randi(16,1,1) - 1;
    [T,R] = trajectory(trajectory_length,x0,pi,D,M,K,h,c,pr);
    for i=1:trajectory_length
        xk = T(i);
        
        eta_c = eta(count_x(xk + 1),1);
        coming_rewards = R(i:trajectory_length);
        sum_rewards_actualized = sum(gamma.^((1:length(coming_rewards))-1).*coming_rewards);
        
        V(xk + 1) = (1-eta_c)*V(xk + 1) + eta_c*(sum_rewards_actualized);
        count_x(xk + 1) = count_x(xk + 1)+1;
        if (xk == 3)
           plot = [plot, V(xk + 1)];
        end
    end
end

end


