function [X,R] = trajectory(n,x0,pi,D,M,K,h,c,pr)
% genere a trajectory X of length  n in the MDP 
% X sequence of states
% R sequence of associated rewards  
% x0 initial state
% pi policy
X = zeros(1,n);
X(1) = x0;
R = zeros(1,n-1);

for i = 1:n
    d = simu(D);
    x = X(i);
    a = pi(x);
    
    X(i+1) = Nextstate(x,a,d,M) + 1;
    R(i) = Reward(x - 1,a,d,M,K,h,c,pr);
end

end

