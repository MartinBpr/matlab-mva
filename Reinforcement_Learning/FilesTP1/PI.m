function [V, pol] = PI(P,R,gamma,pi0,nb_it,M)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

Q = zeros(1, M+1);
pol = pi0;

for k =1:nb_it
    pol_ = pol;
    V = pol_eval1(P,R,pol_,gamma,M);
    for x = 0:M
        for a = 0:M
            sum = 0;
            for y = 0:M
                sum = sum + P(1+x,1+y,1+a)*V(1+y);
            end
            Q(1+x,1+a) = R(1+x,1+a) + gamma*sum;
        end
        [~, ind_max] = max(Q(1+x,1:M+1));
        pol_(1+x) = ind_max - 1;
    end
    pol = pol_;
end
V = transpose(pol_eval1(P,R,pol,gamma,M));
pol = transpose(pol);
end


