function [V, plot] = TD0(pi,n_trajectory,max_length_trajectory,D,M,K,h,c,pr,gamma)
%TD0 Computation of the expectation of V thanks to TD0 algorithm
%

V = ones(1,M+1)*1;
count_x = ones(1,M+1);

plot = [];

for n=1:n_trajectory
    trajectory_length = randi([10,max_length_trajectory]);
    x0 = randi(16,1,1) - 1;
    [T,R] = trajectory(trajectory_length,x0,pi,D,M,K,h,c,pr);
    
    for i=1:trajectory_length
        xk = T(i);
        xk_plus_1 = T(i+1);
        
        eta_c = eta(count_x(xk + 1),0.5);
        
        V(xk + 1) = (1-eta_c)*V(xk + 1) + eta_c*(R(i) + gamma*V(xk_plus_1 + 1));
        count_x(xk + 1) = count_x(xk + 1)+1;
        if (xk == 3)
           plot = [plot, V(xk + 1)];
        end
    end
end

end


