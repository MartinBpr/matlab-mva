function [T, R, A] = simul_random_trajectory(trajectory_length,D,M,K,h,c,pr)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here

T = zeros(1,trajectory_length+1);
A = zeros(1,trajectory_length);
R = zeros(1,trajectory_length);

x0 = randi(15);
last_x = x0;
T(1) = x0;
for i=2:trajectory_length+1
    d = simu(D);
    %a = randi(M-last_x) - 1;
    a = randi(M) - 1;

    x_i = Nextstate(last_x,a,d,M);
    A(i-1) = a;
    T(i) = x_i;
    R(i-1) = Reward(last_x,a,d,M,K,h,c,pr);
    last_x = x_i;
end

end

