function [V pol] = IP_MC(P,R,D,K,h,c,pr,gamma,pi0,nb_it)
% Policy Iteration o� option correspond � la mani�re d'�valuer la 
% politique (option d'�valuation dans pol_eval)

pol=pi0;
M=length(R(:,1))-1;
X=1:(M+1);

for n=1:nb_it
    % evaluation de la politique
    V_pol=pol_evalMC(pol,M,D,K,h,c,pr,gamma);
    % Q valeur de cette politique
    for a=0:M
        Q(X,1+a) = R(X,1+a) + gamma * P(X,:,1+a)*V_pol;
    end
    [V pol] = max(Q,[],2);
    pol = pol-1;
end
    