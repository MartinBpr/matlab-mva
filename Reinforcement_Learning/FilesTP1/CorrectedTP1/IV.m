function[V pol]=IV(P,R,gamma,V0,nb_it)

M=length(R(:,1)) - 1;
X=1:(M+1);
V=V0;


for n=1:nb_it
    % calcul de la Q valeur
    for a=0:M
        Q(X,1+a)=R(X,1+a) + gamma*P(:,:,1+a)*V(X);
    end
    [V pol]=max(Q,[],2);
end

pol = pol - 1;

end

   
   