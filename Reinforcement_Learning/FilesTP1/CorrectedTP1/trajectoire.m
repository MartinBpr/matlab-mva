function [X,R] = trajectoire(n,x0,pi,D,M,K,h,c,pr)
% genere une trajectoire X de longueur n dans le MDP et r le vecteur 
% du profit cumul� discount� 
% pi politique 

cur_state=x0;% stock initial

X=zeros(1,n);
R=zeros(1,n);

for t=1:n
    a=pi(cur_state +1);
    d=simu(D);
    R(t)=Reward(cur_state,a,d,M,K,h,c,pr);
    cur_state=Nextstate(cur_state,a,d,M);
    X(t)=cur_state;
end
    

end

