function [V]= pol_evalMC(pol,M,D,K,h,c,pr,gamma)     
% policy evaluation using Monte Carlo

nb_traj=100; % choix d'un nb de simulations pour l'estimateur de Monte-Carlo
n = 100; % choix d'une longeur de trajectoire � partir de x
Samples=zeros(M+1,nb_traj);   
     
for j=1:nb_traj
    for x=0:M
        % generation d'un trajectoire de longueur n � partir de x 
        % et r�compense discount�e associ�e           
        [X,R] = trajectoire(n,x,pol,D,M,K,h,c,pr); 
        Samples(1+x,j) = sum(gamma.^(0:99).*R);
    end
end

V=mean(Samples,2);