function [V] =pol_eval(pol,P,R,M,gamma,option)
% option 1 : calcul direct (inversion matrice)
% option 2 : calcul par point fixe de T_pi - IV - 


switch option
    
    case 1

        Ppol=zeros(M+1,M+1);
        Rpol=zeros(M+1,1);

        Y=1:(M+1);
        for x=0:M
            Ppol(1+x,Y) = P(1+x,Y,1+pol(1+x));
            Rpol(1+x)= R(1+x,1+pol(1+x));
        end

        V = (eye(M+1) -gamma*Ppol) \ Rpol;
 
    case 2

        V = zeros(M+1,1);% initialisation
        W = zeros(M+1,1);%vecteur temporaire
        for n=1:100 % choix d'un nb d'itérations 
            for x=0:M
                W(1+x) = R(1+x,1+pol(1+x)) + gamma * P(1+x,:,1+pol(1+x))*V;
            end
            V=W;
        end

       
 
end


 

