function [Q pol] = Q_Learning(P,R,Q0,nb_it,D,M,K,h,c,pr,gamma)

Q=Q0; % initialisation
N=ones(M+1,M+1); % nombre de visite de chaque couple etat/action
x=1; 
a=0;

for t=1:nb_it
   % choix de l'action a 
   a=mod(a+1,M+1);
   N(1+x,1+a)=N(1+x,1+a)+1;
   % observation de la r�compense et l'�tat suivant
   d=simu(D);
   y=Nextstate(x,a,d,M);
   r=Reward(x,a,d,M,K,h,c,pr);
   % mise � jour de la table des Q-valeurs et nouvel �tat
   Q(1+x,1+a)=(1-1/N(1+x,1+a))*Q(1+x,1+a)+(1/N(1+x,1+a))*(r + gamma*max(Q(1+y,:)));
   x=y;
end


pol=zeros(1,M+1);
for i=0:M
    [m,pol(i+1)]=max(Q(1+i,:));
    pol(i+1)=pol(i+1)-1;
end

end
