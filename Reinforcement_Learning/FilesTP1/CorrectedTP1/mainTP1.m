%% Problem setting 

M = 15; % taille du stock
K = 0.8; % prix de livraison
c = 0.5; % prix d'achat
h = 0.3; % cout d'entretien
pr = 1; % prix de vente
gamma = 0.98; % calcul a partir du taux d'inflation
 
p = 0.1; % parameter of the geometric distribution
D=zeros(1,M+1); % vector of the geometric distribution
D(1+(0:(M-1)))=p*(1-p).^(0:(M-1));
D(M+1)=1-sum(D);

x0=M; % stock initial

%% Visualisation d'une trajectoire

%pi=2*ones(1,M+1);%politique qui ach�te toujours deux machines
%pi=15:-1:0; %politique qui remplit le stoc � chaque fois
pi=zeros(1,M+1);pi(1)=15;pi(2)=14;


x0=0;

n=1000;

[X,R] = trajectoire(n,x0,pi,D,M,K,h,c,pr);



% visualisation du profit discounte jusqu'au temps n sur une trajectoire
plot(cumsum(gamma.^((1:n)-1).*R))

%%  Estimation du profit moyen cumule discounte

x0=0;

N=1000;
Sample=zeros(N,n);
for i=1:N
    [X,R] = trajectoire(n,x0,pi,D,M,K,h,c,pr);
    Sample(i,:)=cumsum(gamma.^((1:n)-1).*R);
end

Profit=mean(Sample,1);

plot(Profit);


%% Calcul des param�tres du MDP

[P,R]=MDP(D,M,K,h,c,pr);


%% Value iteration

nb_it=1000;
V0=zeros(M+1,1);

tic
[V pol]=IV(P,R,gamma,V0,nb_it);
toc


%% Policy iteration 

nb_it=1000;
pi0=ones(M+1,1);

tic
[V1 pol1] = IP(P,R,gamma,pi0,nb_it,1);
toc

% MC very slow !
%tic
%[V3 pol3] = IP_MC(P,R,D,K,h,c,pr,gamma,pi0,nb_it);
%toc

%% Vitesse de convergence

K=30;

[Vstar pol]=IV(P,R,gamma,V0,1000);
approx=zeros(1,K);


for k=1:K
    [V pol]=IV(P,R,gamma,V0,k);
    approx(k)=max(abs(V-Vstar));
end

approx1=approx;

for k=1:K
    [V pol]=IP(P,R,gamma,pi0,k,1);
    approx1(k)=max(abs(V-Vstar));
end


plot(1:K,approx,1:K,approx1)


%% Q-Learning

Q0=ones(M+1,M+1);
nb_it=10000000;

tic
[Q pol] = Q_Learning(P,R,Q0,nb_it,D,M,K,h,c,pr,gamma);
toc




