function [V,pol] = IV(P,R,gamma,V0,nb_it,M)
syms y

Q = zeros(1, M+1);
V = V0;
V_ = zeros(1, M+1);
pol = zeros(1, M+1);
for k =1:nb_it
   for x = 0:M
       for a = 0:M
          sum = 0;
          for y = 0:M
              sum = sum + P(1+x,1+y,1+a)*V(1+y);
          end
          Q(1+x,1+a) = R(1+x,1+a) + gamma*sum;
       end
       [xmax, ind_max] = max(Q(1+x,1:M+1));
       if k == nb_it
           pol(1+x) = ind_max - 1;
       end
       V_(1+x) = xmax;
   end
   V = V_;
end
    
end


