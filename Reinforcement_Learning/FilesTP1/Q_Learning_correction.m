function [Q, pol] = Q_Learning_correction(nb_it,D,M,K,h,c,pr,gamma)

Q = zeros(M+1,M+1);
count=ones(M+1,M+1);
x_t=randi(16)-1; 

for t=1:nb_it
   a=randi(16)-1; % we select randomly an action
   d=simu(D);
   
   x_t_plus_1=Nextstate(x_t,a,d,M);
   r = Reward(x_t,a,d,M,K,h,c,pr);
   eta = 1/count(1+x_t,1+a);
   
   Q(1+x_t,1+a)=(1-eta)*Q(1+x_t,1+a) + eta*(r + gamma*max(Q(1+x_t_plus_1,:))); % we update Q
   
   count(1+x_t,1+a)=count(1+x_t,1+a)+1;
   x_t=x_t_plus_1;
end


pol=zeros(1,M+1);
for i=1:M+1
    [~,ind_max]=max(Q(i,:));
    pol(i)=ind_max-1;
end

end
