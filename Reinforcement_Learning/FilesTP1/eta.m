function [eta_t] = eta(t,alpha)
    eta_t = 1/(t^alpha);
end
