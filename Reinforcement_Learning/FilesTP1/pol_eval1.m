function [V] = pol_eval1(P,R,pi,gamma,M)
%UNTITLED2 Summary of this function goes here
%   P matrix of P(xt,xt+1,a)
%   R matrix for R(x,a)
%   pi matrix of policy

R_pi = zeros(1, M+1);

for i = 1:M+1
   R_pi(i) = R(i,pi(i) + 1);
end

P_pi = zeros(M+1);
for i = 1:M+1
    for j = 1:M+1
        P_pi(i,j) = P(i,j,pi(i) + 1);
    end
end

identity = eye(M+1,M+1);

V = inv(identity - gamma .* P_pi) * transpose(R_pi);

end

