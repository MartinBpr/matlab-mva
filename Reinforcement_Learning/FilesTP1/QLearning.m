function [pol, Q, plot] = QLearning(trajectory_length,D,M,K,h,c,pr,gamma)
%UNTITLED12 Summary of this function goes here
%   Detailed explanation goes here

[T,R,A] = simul_random_trajectory(trajectory_length,D,M,K,h,c,pr);

Q = zeros(M+1,M+1);
count = ones(M+1,M+1);
plot = zeros(1,trajectory_length/(25*25));
for i=1:trajectory_length
    x = T(i);
    y = T(i+1);
    a = A(i);
    r = R(i);
    
    %{
    if(x == M)
       x
       a
       y
       r
    end
    %}
    
    eta_c = eta(count(x+1,a+1),0.6);
    Q(x+1,a+1) = (1 - eta_c)*Q(x+1,a+1) + eta_c * (r + gamma*max(Q(y+1,1:M+1)));
    count(x+1,a+1) = count(x+1,a+1) + 1;
    
    if (x == 3 && a == 4)
        if (count(x+1,a+1) < length(plot))
           plot(count(x+1,a+1)-1) = Q(x+1,a+1);
        end
    end
end

pol = zeros(1, M+1);
for x = 0:M 
    [~, ind_max] = max(Q(1+x,1:M+1));
    pol(1+x) = ind_max - 1;
end
end


