function [V, plot] = TD_lambda(lambda,pi,n_trajectory,max_length_trajectory,D,M,K,h,c,pr,gamma)
%TDlambda Computation of the expectation of V thanks to TDlambda algorithm
%

V = ones(1,M+1)*1;
count_x = ones(1,M+1);

plot = [];

for n=1:n_trajectory
    trajectory_length = randi([10,max_length_trajectory]);
    x0 = randi(16,1,1) - 1;
    [T,R] = trajectory(trajectory_length,x0,pi,D,M,K,h,c,pr);

    for i=1:trajectory_length
        xk = T(i);
        eta_c = eta(count_x(xk + 1),0.5);
        
        
        %sum_rewards_actualized = sum(gamma.^((1:length(coming_rewards))-1).*coming_rewards);
        sum = 0;
        for j=i:trajectory_length
            xt = T(j) + 1;
            xt_plus_1 = T(j+1) + 1;
            r = R(j);
            
            sum = sum + lambda^(j-i) * (gamma^(j-i)*(r + gamma*V(xt_plus_1) - V(xt)));
        end
        
        V(xk + 1) = V(xk + 1) + eta_c * sum;
        count_x(xk + 1) = count_x(xk + 1)+1;
        if (xk == 3)
           plot = [plot, V(xk + 1)];
        end
    end
end

end




