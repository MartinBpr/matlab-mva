function [Actions, Rewards, Weights ] = EWFplay(n, G, eta, Seq)
Actions = zeros(1, n);
Rewards = zeros(1, n);

[nbActionsA, ~] = size(G);
ewf = EWF(eta, nbActionsA);
    for t = 1:n
        a = ewf.play();
        b = Seq(t);
        
        r = G(a,b);

        ewf.getReward(transpose(G(:,b)));
        Actions(t) = a;
        Rewards(t) = r;
    end
Weights = ewf.w;
end

