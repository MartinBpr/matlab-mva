%% Adversarial Problem setting 

G = [2 -1  ; 0  1] % gain matrix for player A
n=500; % horizon 


%% Oblivious adversary (EWF and EXP3)

best_value = sqrt(2*log(2)/((exp(1)-1)*n))

eta = best_value;% 0.5;
beta = best_value;% 0.25;


% Define the Sequence of Player B
[nbActionsA, nbActionsB] = size(G);
%Seq = randi(nbActionsB,1,n);
%Seq = ones(1,n);
%Seq = 2*ones(1,n);
Seq = cat(2,2*ones(1,n/4), 1*ones(1,3*n/4));

[a_wf, Rewards_EWF, Weights_EWF] = EWFplay(n, G, eta, Seq);
[a_exp3, Rewards_EXP3, Weights_EXP3] = EXP3play(n, G, eta, beta, Seq);

%Weights_EWF
%Weights_EXP3

top_Rewards = zeros(1,n);
for t = 1:n
    max_reward = -Inf;
    for b = nbActionsB
        
    end
    top_Rewards(t) = max(G(:,Seq(t)));
end

Regret_EWF = cumsum(top_Rewards - Rewards_EWF);
Regret_EXP3 = cumsum(top_Rewards - Rewards_EXP3);

%subplot(1,2,1)
plot(1:n,Regret_EWF,1:n,Regret_EXP3)
legend('EWF', 'EXP3', 'Location','northwest')
xlabel('Iterations')
ylabel('Regret')

%subplot(1,2,2)
%plot(1:n,a_wf+2,1:n,a_exp3)
%ylim([0.5 4.5])
%legend('EWF', 'EXP3')



%% EXP 3 versus EXP 3 : nash equilibrium
%{
n=5000
best_value = sqrt(2*log(2)/((exp(1)-1)*n))

eta = best_value;% 0.5;
beta = best_value*100;% 0.25;

[ ActionsA, ActionsB, RewardsA ] = EXP3vEXP3(n, eta, beta, G );
pa = cumsum(ActionsA == 1)./(1:n);
pb = cumsum(ActionsB == 1)./(1:n);

subplot(1,2,1)
plot(1:n, pa, 1:n,pb)
legend('p_a', 'p_b')
xlabel('iterations')

subplot(1,2,2)
plot(cumsum(RewardsA)./(1:n))
legend('Value of the game for A')
xlabel('iterations')
%}
%% EXP3 versus Thompson Sampling
%{
Arm1=armBernoulli(0.2);
Arm2=armBernoulli(0.4);
Arm3=armBernoulli(0.5);

MAB={Arm1,Arm2,Arm3};

Means=[Arm1.mean Arm2.mean Arm3.mean];
mumax=max(Means);

eta=0.01;
beta=0.1;

N=500;
n=1000;

% Estimated cumulated regret up to time n

Reg1=zeros(1,n);
Reg2=zeros(1,n);

compt=0;

for i=1:N
    [~,rew1]= Thompson(n,MAB);
    % complete EXP3stochastic 
    [~,rew2]= EXP3stochastic(n,beta,eta,MAB);
    Reg1=Reg1 + (1:n)*mumax - cumsum(rew1);
    Reg2=Reg2 + (1:n)*mumax - cumsum(rew2);
    compt=compt+1
end

Reg1=Reg1/N;
Reg2=Reg2/N;

plot(1:n,Reg1,1:n,Reg2)
legend('Thompson Sampling', 'EXP3')

%}
    
    
    

