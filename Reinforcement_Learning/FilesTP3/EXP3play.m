function [Actions, Rewards, Weights  ] = EXP3play(n, G, eta, beta, Seq)
Actions = zeros(1, n);
Rewards = zeros(1, n);

[nbActionsA, ~] = size(G);
exp3 = EXP3(eta, beta, nbActionsA);
    for t = 1:n
        a = exp3.play();
        b = Seq(t);
        
        r = G(a,b);
        
        exp3.getReward(r);
        Actions(t) = a;
        Rewards(t) = r;
    end
    
Weights = exp3.w;
end

