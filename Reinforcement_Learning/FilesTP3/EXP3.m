classdef EXP3<handle
    % EXP 3 strategy for one player
    
    properties
        nbActions
        eta % learning rate
        beta % exploration parameter
        w 
        lastAction
        p_
    end
    
    methods
        
        function self = EXP3(eta,beta,nbActions)
            self.eta = eta;
            self.beta = beta;
            self.nbActions = nbActions;
            self.w = ones(1, nbActions);
        end
        
        function self = init(self)
            
        end
        
        function [action] = play(self)
            % chooses the next action
            w_sum = sum(self.w);
            self.p_ = ((1 - self.beta) / w_sum * self.w) + self.beta/self.nbActions;
            
            action = simu(self.p_);
            self.lastAction=action;
        end
        
        function self = getReward(self,r)
            % r the last rewards recieved by A
            r_ = r / self.p_(self.lastAction);
            self.w(self.lastAction) = self.w(self.lastAction) * exp(self.eta * r_);
            
         end
                
    end    
end
