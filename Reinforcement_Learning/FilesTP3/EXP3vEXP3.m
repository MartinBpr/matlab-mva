function [ ActionsA, ActionsB, RewardsA ] = EXP3vEXP3(n, eta, beta, G )

ActionsA = zeros(1, n);
ActionsB = zeros(1, n);
RewardsA = zeros(1, n);

[nbActionsA, nbActionsB] = size(G);
exp3A = EXP3(eta, beta, nbActionsA);
exp3B = EXP3(eta, beta, nbActionsB);
for t = 1:n
        a = exp3A.play();
        b = exp3B.play();
        
        r = G(a,b);
        
        exp3A.getReward(r);
        exp3B.getReward(-r);
        
        ActionsA(t) = a;
        ActionsB(t) = b;
        RewardsA(t) = r;
end
end

